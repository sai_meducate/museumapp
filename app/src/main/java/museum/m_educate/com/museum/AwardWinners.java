package museum.m_educate.com.museum;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by DELL on 11/6/2017.
 */

public class AwardWinners extends AppCompatActivity {
    Button gallantry,padma_bhushan_pvsm,pvc,mvc,kc,ac,ysm_vc,veer_chakra,shaurya_chakra,home,back,exit;
    private String audio_in = "Hindi";


    final int[] slider_fourteen={R.drawable.wall_fourteen_one_two,R.drawable.wall_fourteen_two,R.drawable.wall_fourteen_four};
    final int[] slider_fifthteen={R.drawable.wall_fifteen_one,R.drawable.wall_sixteen_one};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.awardwinners);

        gallantry = (Button) findViewById(R.id.gallantry);
        padma_bhushan_pvsm = (Button) findViewById(R.id.padma_bhushan);
        pvc = (Button) findViewById(R.id.pvc);
        mvc = (Button) findViewById(R.id.mvc);
        kc= (Button) findViewById(R.id.kc);
        ac = (Button) findViewById(R.id.ac);
        ysm_vc = (Button) findViewById(R.id.ysm_vc);
        veer_chakra = (Button) findViewById(R.id.veer_chakra);
//        shaurya_chakra = (Button) findViewById(R.id.shaurya_chakra);
        back = (Button) findViewById(R.id.back_button);
        home = (Button) findViewById(R.id.cont);
        exit = (Button) findViewById(R.id.exit);

        try {
            audio_in = getIntent().getStringExtra("audio_in");
        } catch (Exception e) {
            Toast.makeText(this, "Audio not selected", Toast.LENGTH_LONG).show();
        }
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(AwardWinners.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(AwardWinners.this, GroupTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(AwardWinners.this);

                alert.setTitle("Are you sure you want to close this tour ?");

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }


                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });

        gallantry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                wall8
                Intent environmentPage = new Intent(AwardWinners.this, WallExplanation.class);
//               pass to wall8 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_fourteen_chakras);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_fourteen_chakras);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("sliders",slider_fourteen);
                environmentPage.putExtra("position", 13);
                environmentPage.putExtra("from","awardwinners");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });

        padma_bhushan_pvsm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                pass to wall9 raw file data
                Intent environmentPage = new Intent(AwardWinners.this, WallExplanation.class);
                environmentPage.putExtra("audio_english",R.raw.wall_eng_sixtyfour_chakras);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_sixtyfour_chakras);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("from","awardwinners");
                environmentPage.putExtra("position", 0);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
//wall9
            }
        });

        pvc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(AwardWinners.this, WallExplanation.class);
//               pass to wall10 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_one);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_one);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("from","awardwinners");
                environmentPage.putExtra("position", 0);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();

            }
        });
        mvc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(AwardWinners.this, WallExplanation.class);
//               pass to wall10 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_one);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_one);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("from","awardwinners");
                environmentPage.putExtra("position", 0);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
        kc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(AwardWinners.this, WallExplanation.class);
//               pass to wall8 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_one);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_one);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("from","awardwinners");
                environmentPage.putExtra("position", 0);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
        ac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(AwardWinners.this, WallExplanation.class);
//               pass to wall8 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_one);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_one);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("from","awardwinners");
                environmentPage.putExtra("position", 0);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
        ysm_vc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(AwardWinners.this, WallExplanation.class);
//               pass to wall8 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_one);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_one);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("from","awardwinners");
                environmentPage.putExtra("position", 0);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
        veer_chakra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(AwardWinners.this, WallExplanation.class);
//               pass to wall8 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_fiftheen_and_sixteen_veer_shaurya_chakra);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_fiftheen_and_sixteen_veer_shaurya_chakra);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("sliders",slider_fifthteen);
                environmentPage.putExtra("from","awardwinners");
                environmentPage.putExtra("position", 14);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
//        shaurya_chakra.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent environmentPage = new Intent(AwardWinners.this, WallExplanation.class);
////               pass to wall8 raw file data
//                environmentPage.putExtra("audio_english",R.raw.wall_eng_one);
//                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_one);
//                environmentPage.putExtra("image",R.drawable.no_preview);
//                environmentPage.putExtra("audio_in",audio_in);
//                environmentPage.putExtra("from","awardwinners");
//                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(environmentPage);
//                finish();
//            }
//        });
    }

    @Override
    public void onBackPressed() {
        Intent environmentPage = new Intent(AwardWinners.this, GroupTourActivity.class);
        environmentPage.putExtra("audio_in", audio_in);
        startActivity(environmentPage);
        finish();
    }
}
