package museum.m_educate.com.museum;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class GoroupSequenceTourActivity extends AppCompatActivity {
    Button buttoneng, buttonhindi;
    private String audio_in="Hindi";
    Button back, home,exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.group_sequence_tour);

        buttoneng = (Button) findViewById(R.id.button);
        buttoneng.setText(Html.fromHtml("<b> "
                + "SEQUENCE TOUR" + "<br/><font size=6px>"
                + "(Walls in sequence order)"+"</font><br/>"
                + "  </b>"));


        buttonhindi = (Button) findViewById(R.id.button1);
        buttonhindi.setText(Html.fromHtml("<b> "
                + "CUSTOMIZED TOUR" + "<br/><font size=6px>"
                + "(Customized according to groups)"+"</font><br/>"
                + "  </b>"));
        exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(GoroupSequenceTourActivity.this);

                alert.setTitle("Are you sure you want to close this tour ?");

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       finish();
                    }


                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });
        try {
            audio_in = getIntent().getStringExtra("audio_in");

        } catch (Exception e) {
            Toast.makeText(this, "Audio not selected", Toast.LENGTH_LONG).show();
        }
        buttoneng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GoroupSequenceTourActivity.this, SequencialTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });

        buttonhindi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GoroupSequenceTourActivity.this, GroupTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        back = (Button) findViewById(R.id.back_button);
        home = (Button) findViewById(R.id.cont);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GoroupSequenceTourActivity.this, MainActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GoroupSequenceTourActivity.this, IndonesianEmblemActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });



    }

    @Override
    public void onBackPressed() {
        Intent environmentPage = new Intent(GoroupSequenceTourActivity.this, IndonesianEmblemActivity.class);
        environmentPage.putExtra("audio_in", audio_in);
        startActivity(environmentPage);
        finish();
    }
}