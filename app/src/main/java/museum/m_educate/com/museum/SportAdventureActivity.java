package museum.m_educate.com.museum;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by DELL on 11/7/2017.
 */

public class SportAdventureActivity extends AppCompatActivity {

    Button sports,adventure,home,back,exit;
    private String audio_in = "Hindi";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.sportadvantureactivity);

        sports = (Button) findViewById(R.id.sports);
        adventure = (Button) findViewById(R.id.adventure);
        back = (Button) findViewById(R.id.back_button);
        home = (Button) findViewById(R.id.cont);
        exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(SportAdventureActivity.this);

                alert.setTitle("Are you sure you want to close this tour ?");

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }


                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });


        try {
            audio_in = getIntent().getStringExtra("audio_in");

        } catch (Exception e) {
            Toast.makeText(this, "Audio not selected", Toast.LENGTH_LONG).show();
        }

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(SportAdventureActivity.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(SportAdventureActivity.this, GroupTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        sports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                wall8
                Intent environmentPage = new Intent(SportAdventureActivity.this, WallExplanation.class);
//               pass to wall 52 and 53 raw file data
                environmentPage.putExtra("audio_english",R.raw.customized_sports);
                environmentPage.putExtra("audio_hindi",R.raw.customized_sports);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("position", 4);

                environmentPage.putExtra("from","sportadventureactivity");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });

        adventure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                pass to wall 48 raw file data
                Intent environmentPage = new Intent(SportAdventureActivity.this, WallExplanation.class);
                environmentPage.putExtra("audio_english",R.raw.wall_eng_fiftyfour_two_adventure);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_fiftyfour_two_adventure);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("position", 53);

                environmentPage.putExtra("from","sportadventureactivity");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent environmentPage = new Intent(SportAdventureActivity.this, GroupTourActivity.class);
        environmentPage.putExtra("audio_in", audio_in);
        startActivity(environmentPage);
        finish();
    }
}
