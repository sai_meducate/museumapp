package museum.m_educate.com.museum;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by macbookpro on 10/10/17.
 */

public class UnitsDataAdapter extends RecyclerView.Adapter<UnitsDataAdapter.ViewHolder> {
    private ArrayList<Units> units;
    private Context context;
    private String audio_in;
    final int[] gradient={R.drawable.gradient_one
//            R.drawable.gradient_two,
//            R.drawable.gradient_three,
//            R.drawable.gradient_four,
//            R.drawable.gradient_five,
          };

    public UnitsDataAdapter(Context context, ArrayList<Units> units, String audio_in) {
        this.units = units;
        this.context = context;
        this.audio_in = audio_in;
    }

    @Override
    public UnitsDataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.unit_row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(UnitsDataAdapter.ViewHolder viewHolder, int i) {

        viewHolder.tv_android.setText(units.get(i).getWall());
        viewHolder.img_android.setBackgroundResource(gradient[new Random().nextInt(gradient.length)]);

//        viewHolder.img_android.setBackgroundResource(R.drawable.no_prev);
    }

    @Override
    public int getItemCount() {
        return units.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_android;
        private FrameLayout img_android;
        public ViewHolder(View view) {

            super(view);
            final int[] audio_eng={
                    R.raw.wall_eng_three,
                    R.raw.wall_eng_four,
                    R.raw.wall_eng_five,

                    R.raw.wall_eng_six,
                    R.raw.wall_eng_seven,
                    R.raw.wall_eng_thirtyone,

                    R.raw.wall_eng_thirtytwo,
                    R.raw.wall_eng_thirtythree,
                    R.raw.wall_eng_thirtyfour,

                    R.raw.wall_eng_thirtyfive,
                    //static data
                    R.raw.wall_eng_thirtysix,
                    R.raw.wall_eng_thirtyseven,

                    R.raw.wall_eng_thirtyeight,
                    R.raw.wall_eng_thirtynine,
                    R.raw.wall_eng_fourty,

                    R.raw.wall_eng_fourtyone,
                    R.raw.wall_eng_fourtytwo,
                    R.raw.wall_eng_fourtythree,
//change below this audio to wall_eng_fourtyfour
                    R.raw.wall_eng_fourtyfournew,
                    R.raw.wall_eng_fourtyfive,
                    R.raw.wall_eng_fourtysix,

                    // wall 47 not available
                    R.raw.wall_eng_fiftynine,
                    R.raw.wall_eng_sixty,
                    R.raw.wall_eng_sixtyone,
//                    no wall no

                    // done
                    R.raw.wall_eng_seventytwo_two_twentyone_rr };


            final int[] audio_hindi=
                    {
                            R.raw.wall_eng_three,
                            R.raw.wall_eng_four,
                            R.raw.wall_eng_five,
                            R.raw.wall_eng_six,
                            R.raw.wall_eng_seven,
                            R.raw.wall_eng_thirtyone,
                            R.raw.wall_eng_thirtytwo,
                            R.raw.wall_eng_thirtythree,
                            R.raw.wall_eng_thirtyfour,
                            R.raw.wall_eng_thirtyfive,
                            //static data
                            R.raw.wall_eng_thirtysix,
                            R.raw.wall_eng_thirtyseven,
                            R.raw.wall_eng_thirtyeight,
                            R.raw.wall_eng_thirtynine,
                            R.raw.wall_eng_fourty,
                            R.raw.wall_eng_fourtyone,
                            R.raw.wall_eng_fourtytwo,
                            R.raw.wall_eng_fourtythree,
                            R.raw.wall_eng_fourtyfournew,
                            R.raw.wall_eng_fourtyfive,
                            R.raw.wall_eng_fourtysix,
                            // wall 47 not available
                            R.raw.wall_eng_fiftynine,

//                    no wall no
                            R.raw.wall_eng_sixty,
                            R.raw.wall_eng_sixtyone,
                            // done
                            R.raw.wall_eng_seventytwo_two_twentyone_rr

                    };

            final int[] slider_default={R.drawable.noimage,R.drawable.noimage_one,R.drawable.noimage_two};
            final int[] slider_two={R.drawable.wall_two_one,R.drawable.wall_two_two,R.drawable.wall_two_three};
            final int[] slider_three={R.drawable.wall_three_one,R.drawable.wall_three_two,R.drawable.wall_three_three,R.drawable.wall_three_four};
            final int[] slider_four={R.drawable.wall_four_one,R.drawable.wall_four_two_two,R.drawable.wall_four_three,R.drawable.wall_four_four,R.drawable.wall_four_five};
            final int[] slider_five={R.drawable.wall_five_one_two,R.drawable.wall_five_two_two,R.drawable.wall_five_three,R.drawable.wall_five_four_two};
            final int[] slider_six={R.drawable.wall_six_one,R.drawable.wall_six_two,R.drawable.wall_six_three,R.drawable.wall_six_four};
            final int[] slider_seven={R.drawable.wall_seven_one_two,R.drawable.wall_seven_two_two,R.drawable.wall_seven_three_two,R.drawable.wall_seven_four,R.drawable.wall_seven_five,R.drawable.wall_seven_eleven};
            final int[] slider_eight={R.drawable.wall_eight_one};
            final int[] slider_nine={R.drawable.wall_nine_one};
            final int[] slider_ten={R.drawable.wall_ten_two,R.drawable.wall_ten_three};

            final int[] slider_eleven={R.drawable.wall_eleven_two,R.drawable.wall_eleven_three};
            final int[] slider_twelve={R.drawable.noimage,R.drawable.noimage,R.drawable.noimage};
            final int[] slider_thirteen={R.drawable.wall_thirteen_two};
            final int[] slider_fourteen={R.drawable.wall_fourteen_one_two,R.drawable.wall_fourteen_two,R.drawable.wall_fourteen_four};
            final int[] slider_fifthteen={R.drawable.wall_fifteen_one,R.drawable.wall_sixteen_one};
//            final int[] slider_sixteen={R.drawable.wall_sixteen_one};
            final int[] slider_seventeen={R.drawable.wall_seventeen_one};
            final int[] slider_eighteen={R.drawable.wall_eighteen_one};
            final int[] slider_nineteen={R.drawable.wall_ninteen_one};
            final int[] slider_twenty={R.drawable.wall_twenty_point_one,R.drawable.wall_twenty_point_two,R.drawable.wall_twenty_point_three};

            final int[] slider_twentyone={R.drawable.wall_twentyone_one};
            final int[] slider_twentytwo={R.drawable.wall_twentytwo_one};

            final int[] slider_twentythree={R.drawable.wall_twentythree_one,R.drawable.wall_twentyfour_one,R.drawable.wall_twentyfive,R.drawable.wall_twentysix_one
            ,R.drawable.wall_twentyseven_one,R.drawable.wall_twentyeight_one,R.drawable.wall_twentynine_one,
                    R.drawable.wall_thirty_point_one,R.drawable.wall_thirty_point_two,R.drawable.wall_thirty_point_three};

//            final int[] slider_twentyfour={R.drawable.wall_twentyfour_one};
//            final int[] slider_twentyfive={R.drawable.wall_twentyfive};
//            final int[] slider_twentysix={R.drawable.wall_twentysix_one};
//            final int[] slider_twentyseven={R.drawable.wall_twentyseven_one};
//            final int[] slider_twentyeight={R.drawable.wall_twentyeight_one};
//            final int[] slider_twentynine={R.drawable.wall_twentynine_one};
//            final int[] slider_thirty={R.drawable.wall_thirty_point_one,R.drawable.wall_thirty_point_two,R.drawable.wall_thirty_point_three};

            final int[] slider_thirtyone={R.drawable.wall_thirtyone_one_two,R.drawable.wall_thirtyone_two,R.drawable.wall_thirtyone_three,R.drawable.wall_thirtyone_four};
            final int[] slider_thirtytwo={R.drawable.wall_thirtytwo_one_two,R.drawable.wall_thirtytwo_two};
            final int[] slider_thirtythree={R.drawable.wall_thirtythree_one_two,R.drawable.wall_thirtythree_two};
            final int[] slider_thirtyfour={R.drawable.wall_thirtyfour_one,R.drawable.wall_thirtyfour_two};
            final int[] slider_thirtyfive={R.drawable.wall_thirtyfive_one,R.drawable.wall_thirtyfive_two};
            final int[] slider_thirtysix={R.drawable.wall_thirtysix_one,R.drawable.wall_thirtysix_two};
            final int[] slider_thirtyseven={R.drawable.wall_thirtyseven_two};
            final int[] slider_thirtyeight={R.drawable.wall_thirtyeight_one,R.drawable.wall_thirtyeight_two};
            final int[] slider_thirtynine={R.drawable.wall_thirtynine_one,R.drawable.wall_thirtynine_two,R.drawable.wall_thirtynine_three,R.drawable.wall_thirtynine_four};
            final int[] slider_fourty={R.drawable.wall_fourty_point_one,R.drawable.wall_fourty_point_two};

            final int[] slider_fourtyone={R.drawable.wall_fourtyone_one,R.drawable.wall_fourtyone_two};
            final int[] slider_fourtytwo={R.drawable.noimage,R.drawable.noimage,R.drawable.noimage};
            final int[] slider_fourtythree={R.drawable.wall_fourtythree_one,R.drawable.wall_fourtythree_two,R.drawable.wall_fourtythree_four,R.drawable.wall_fourtythree_five};
            final int[] slider_fourtyfour={R.drawable.wall_fourtyfour_one};
            final int[] slider_fourtyfive={R.drawable.wall_fourtyfive_one,R.drawable.wall_fourtyfive_two};
            final int[] slider_fourtysix={R.drawable.wall_fourtysix_one,R.drawable.wall_fourtysix_two,R.drawable.wall_fourtysix_four,R.drawable.wall_fourtysix_five,R.drawable.wall_fourtysix_seven};
            final int[] slider_fourtyseven={R.drawable.wall_fourtyseven_one};
            final int[] slider_fourtyeight={R.drawable.wall_fourtyeight_one,R.drawable.wall_fourtyeight_two};
            final int[] slider_fourtynine={R.drawable.wall_fourtynine_one,R.drawable.wall_fourtynine_two,R.drawable.wall_fourtynine_three,R.drawable.wall_fourtynine_four,R.drawable.wall_fourtynine_five};



            tv_android = (TextView)view.findViewById(R.id.tv_android);
            img_android = (FrameLayout) view.findViewById(R.id.img_android);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(getAdapterPosition()==0) {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[0]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[0]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_three);
                        environmentPage.putExtra("position", 0);

                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }else if(getAdapterPosition()==1)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[1]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[1]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_four);
                        environmentPage.putExtra("position", 1);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    //static data
                    else if(getAdapterPosition()==2)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[2]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[2]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_five);
                        environmentPage.putExtra("position", 2);

                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==3)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[3]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[3]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_six);
                        environmentPage.putExtra("position", 3);

                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==4)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[4]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[4]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_seven);
                        environmentPage.putExtra("position", 4);

                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==5)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[5]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[5]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 5);

                        environmentPage.putExtra("sliders",slider_thirtyone);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==6)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[6]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[6]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 6);

                        environmentPage.putExtra("sliders",slider_thirtytwo);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==7)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[7]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[7]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 7);

                        environmentPage.putExtra("sliders",slider_thirtythree);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==8)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[8]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[8]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_thirtyfour);
                        environmentPage.putExtra("position", 8);

                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==9)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[9]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[9]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 9);

                        environmentPage.putExtra("sliders",slider_thirtyfive);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==10)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[10]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[10]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_thirtysix);
                        environmentPage.putExtra("position", 10);

                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==11)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[11]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[11]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_thirtyseven);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.putExtra("position", 11);

                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==12)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[12]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[12]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_thirtyeight);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.putExtra("position", 12);

                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==13)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[13]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[13]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_thirtynine);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.putExtra("position", 13);

                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==14)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[14]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[14]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_fourty);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.putExtra("position", 14);

                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==15)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[15]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[15]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_fourtyone);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.putExtra("position", 15);

                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }else if(getAdapterPosition()==16)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[16]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[16]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_fourtytwo);
                        environmentPage.putExtra("position", 15);

                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==17)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[17]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[17]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 15);

                        environmentPage.putExtra("sliders",slider_fourtythree);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==18)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[18]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[18]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 15);

                        environmentPage.putExtra("sliders",slider_fourtyfour);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==19)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[19]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[19]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 19);

                        environmentPage.putExtra("sliders",slider_fourtyfive);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==20)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[20]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[20]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 20);

                        environmentPage.putExtra("sliders",slider_fourtysix);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==21)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[21]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[21]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 21);

                        environmentPage.putExtra("sliders",slider_fourtyseven);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==22)
                    {
                        Log.d("22", "onClick: 1");
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[22]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[22]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==23)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[23]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[23]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==24)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[24]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[24]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 24);
                        environmentPage.putExtra("position", 24);

                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if(getAdapterPosition()==25)
                    {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[25]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[25]);
                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 25);

                        environmentPage.putExtra("from", "unit_data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
//                    else if(getAdapterPosition()==26)
//                    {
//                        Intent environmentPage = new Intent(context, WallExplanation.class);
//                        environmentPage.putExtra("audio_english", audio_eng[27]);
//                        environmentPage.putExtra("audio_hindi", audio_hindi[27]);
//                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
//                        environmentPage.putExtra("audio_in", audio_in);
//                        environmentPage.putExtra("from", "unit_data_adapter");
//                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        context.startActivity(environmentPage);
//                    }
//                    else if(getAdapterPosition()==27)
//                    {
//                        Intent environmentPage = new Intent(context, WallExplanation.class);
//                        environmentPage.putExtra("audio_english", audio_eng[28]);
//                        environmentPage.putExtra("audio_hindi", audio_hindi[28]);
//                        environmentPage.putExtra("image", units.get(getAdapterPosition()).getImage_url());
//                        environmentPage.putExtra("audio_in", audio_in);
//                        environmentPage.putExtra("from", "unit_data_adapter");
//                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        context.startActivity(environmentPage);
//                    }
//
                    else{
                        int a=getAdapterPosition()+1;
                        Toast.makeText(context,"Unit "+a+" is currently not available",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

}
