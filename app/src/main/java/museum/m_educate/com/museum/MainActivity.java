package museum.m_educate.com.museum;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;


public class MainActivity extends AppCompatActivity  {
    Button button_language, button_play, button_pause, button_stop, button_continue, button_back;
    SeekBar seekbar;
    TextView totaltime, ongoingtime, jayhind,welcometext;
    final MediaPlayer[] mPlayer2 = new MediaPlayer[1];
    String audio_in = "Hindi";
    Handler seekHandler = new Handler();
    MediaPlayer player;
    private Handler mHandler = new Handler();
    private Timer timer;

    int xaa = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setTitle("Welcome");
        setContentView(R.layout.activity_main);
        final Handler handler = new Handler();

        button_continue = (Button) findViewById(R.id.cont);
        button_language = (Button) findViewById(R.id.language);
        button_back = (Button) findViewById(R.id.back_button);
        welcometext = (TextView) findViewById(R.id.welcometext);
//        button_pause = (Button) findViewById(R.id.pause);
        button_play = (Button) findViewById(R.id.paly);
        button_play.setText("Play");
        button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_play_arrow_black_24dp, 0, 0);
        button_stop = (Button) findViewById(R.id.stop);
        totaltime = (TextView) findViewById(R.id.total_time);
        jayhind = (TextView) findViewById(R.id.textJayhind);
        ongoingtime = (TextView) findViewById(R.id.ongoing_time);
        seekbar = (SeekBar) findViewById(R.id.seekbar);

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean input) {
                if (input){
                    player.stop();
                    Log.d("seek", "onProgressChanged: ");
                    startAudio(progress);

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.d("seek", "onStartTrackingTouch: " + seekBar.getProgress());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d("seek", "onStopTrackingTouch : " + seekBar.getProgress());
            }
        });
//        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                Log.d("onPrepared", "onPrepared: ");
//            }
//        });

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Log.d("run", "run: henggkdfgbkfb " );
                jayhind.setVisibility(View.GONE);
                welcometext.setVisibility(View.VISIBLE);
                        final Runnable runnable = new Runnable() {
                            public void run() {
                                Log.d("run", "run: vgdfgdfhgfh" );
                                    welcometext.setVisibility(View.GONE);

                                }
                        };
                        // trigger first time
                        handler.postDelayed(runnable, 10000);
            }
        }, 3000);


//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//
//            }
//        },4000,15000);

//        startAudio();


        button_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stop();
                Intent environmentPage = new Intent(MainActivity.this, IntrodoctionActivity.class);
                environmentPage.putExtra("audio_in", "English");
                startActivity(environmentPage);
                finish();
            }
        });

        button_play.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean b = true;
                if (button_play.getText().equals("Play") && b == true) {
                    startAudio(0);
                    button_play.setText("Pause");
                    button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_pause_black_24dp, 0, 0);

                    b = false;
                } else if (button_play.getText().equals("Pause")) {
                    xaa = player.getCurrentPosition();
                    player.pause();
                    button_play.setText("Resume");
                    button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_play_arrow_black_24dp, 0, 0);

                    b = false;
                } else if (button_play.getText().equals("Resume") && b == true) {
                    Log.d("resume", "onClick: " + player.getCurrentPosition());
                    player.seekTo(xaa);
                    Log.d("seek", "onClick: "+ xaa);
                    startAudio(xaa);
                    button_play.setText("Pause");
                    button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_pause_black_24dp, 0, 0);

                    b = false;
                }


            }

        });


        button_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button_play.setText("Play");
                button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_play_arrow_black_24dp, 0, 0);
                seekbar.setProgress(0);
                stop();
            }
        });

        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stop();
                System.exit(0);
//                finish();

            }
        });

        button_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("audio", "onClick: " + button_language.getText());
                if (button_language.getText().equals("Language")) {
                    showLanguge();
                } else if (button_language.getText().equals("English")) {
                    stop();
                    audio_in = "English";
                    startAudio(xaa);
                    button_language.setText(getApplicationContext().getString(R.string.hindi_language));

                } else if (button_language.getText().equals("हिंदी")) {
                    stop();
                    audio_in = "Hindi";
                    startAudio(xaa);
                    button_language.setText("English");
                }
            }
        });


    }


    private void showLanguge() {
        final CharSequence[] photo = {"English", "Hindi"};

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Select Language");

        alert.setSingleChoiceItems(photo, -1, new

                DialogInterface.OnClickListener()

                {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (photo[which] == "English")

                        {
                            audio_in = "English";
                            button_language.setText(getApplicationContext().getString(R.string.hindi_language));
                            xaa = 0;
                            stop();
                            startAudio(xaa);
                            dialog.dismiss();

                        } else if (photo[which] == "Hindi")

                        {

                            audio_in = "Hindi";
                            button_language.setText("Listen in English");
                            xaa = 0;
                            stop();
                            startAudio(xaa);
                            dialog.dismiss();

                        }
                    }

                });
        alert.show();
    }

    private void startAudio(int i) {
        if (audio_in.equalsIgnoreCase("Hindi")) {
            player = MediaPlayer.create(this, R.raw.welcome_hindi);
            player.seekTo(i);
        } else {
            player = MediaPlayer.create(this, R.raw.splash_weclcome_english);
            player.seekTo(i);
        }
        player.start();
        button_play.setText("Pause");
        button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_pause_black_24dp, 0, 0);

        Log.d("audio", "startAudio: get duration " + player.getDuration());
        Log.d("audio", "startAudio: get current position " + player.getCurrentPosition());


        seekbar.setMax(player.getDuration());


        totaltime.setText(String.format("%d:%d",
                TimeUnit.MILLISECONDS.toMinutes(player.getDuration()),
                TimeUnit.MILLISECONDS.toSeconds(player.getDuration()) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(player.getDuration()))));

        if (player != null) {
//            player.start();
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (player != null && player.isPlaying()) {
                                ongoingtime.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        ongoingtime.setText(String.format("%d:%d",
                                                TimeUnit.MILLISECONDS.toMinutes(player.getCurrentPosition()),
                                                TimeUnit.MILLISECONDS.toSeconds(player.getCurrentPosition()) -
                                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(player.getCurrentPosition()))));
                                        seekbar.setProgress(player.getCurrentPosition());

                                        Log.d("time", "run: current "+TimeUnit.SECONDS.toSeconds(player.getCurrentPosition()));
                                        Log.d("time", "run: duration "+TimeUnit.SECONDS.toSeconds(player.getDuration()));



                                    }
                                });
                            } else {
                                timer.cancel();
                                timer.purge();
                            }
                        }
                    });
                }
            }, 0, 1000);

            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stop();
                    Intent environmentPage = new Intent(MainActivity.this, IntrodoctionActivity.class);
                    environmentPage.putExtra("audio_in", "English");
                    startActivity(environmentPage);
                    finish();
                }
            });
        }
    }


    private void stop() {
        try {
            if (player.isPlaying() || player != null) {
                player.stop();
                button_play.setText("Play");
                button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_play_arrow_black_24dp, 0, 0);
                seekbar.setProgress(0);
            } else {
                startAudio(0);
                stop();
            }
        } catch (NullPointerException e) {
            Log.d("ttt", "onClick: " + e);
        }

    }



    @Override
    public void onBackPressed() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Are you sure you want to close this tour ?");

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }


        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

}