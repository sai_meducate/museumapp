package museum.m_educate.com.museum;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by macbookpro on 10/10/17.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private ArrayList<Walls> walls;
    private Context context;
    private String audio_in;
    final int[] gradient={R.drawable.gradient_one
//            R.drawable.gradient_two,
//            R.drawable.gradient_three,
//            R.drawable.gradient_four,
//            R.drawable.gradient_five,
    };


    public DataAdapter(Context context, ArrayList<Walls> walls, String audio_in) {
        this.walls = walls;
        this.context = context;
        this.audio_in = audio_in;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder viewHolder, int i) {

        viewHolder.tv_android.setText(walls.get(i).getWall());
        viewHolder.img_android.setBackgroundResource(gradient[new Random().nextInt(gradient.length)]);
        Log.d("audio", "DataAdapter: "+audio_in);

//        viewHolder.img_android.setBackgroundResource(R.drawable.no_prev);
    }


    @Override
    public int getItemCount() {
        return walls.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_android;
        private FrameLayout img_android;
//        int sliders [][]={{R.drawable.garuda_emblem,R.drawable.sld_one},{R.drawable.garuda_emblem,R.drawable.sld_one}};
        public ViewHolder(View view) {

            super(view);
            final int[] audio_eng={

                    R.raw.wall_eng_one,R.raw.archives,R.raw.wall_eng_two,
                    R.raw.wall_eng_three,R.raw.wall_eng_four,R.raw.wall_eng_five,
                    R.raw.wall_eng_six,R.raw.wall_eng_seven,R.raw.wall_eng_eight,
                    R.raw.wall_eng_nine,R.raw.wall_eng_ten,R.raw.wall_eng_eleven,

                    R.raw.wall_eng_twelve,R.raw.wall_eng_thirteen,R.raw.wall_eng_fourteen_chakras,
                    R.raw.wall_eng_fiftheen_and_sixteen_veer_shaurya_chakra,R.raw.wall_eng_seventeen,
                    R.raw.wall_eng_eighteen,R.raw.wall_eng_nineteen,R.raw.wall_eng_twenty,
                    R.raw.wall_eng_twentyone,R.raw.wall_eng_twentytwo,R.raw.wall_eng_twentythree_thirty_honorary_colonel,R.raw.wall_eng_thirtyone,R.raw.wall_eng_thirtytwo,
                    R.raw.wall_eng_thirtythree,R.raw.wall_eng_thirtyfour,R.raw.wall_eng_thirtyfive,
                    R.raw.wall_eng_thirtysix,R.raw.wall_eng_thirtyseven,R.raw.wall_eng_thirtyeight,
                    R.raw.wall_eng_thirtynine,R.raw.wall_eng_fourty,R.raw.wall_eng_fourtyone,
                    R.raw.wall_eng_fourtytwo,R.raw.wall_eng_fourtythree,
                    // wall replace with fourty four
                    R.raw.wall_eng_fourtyfournew,
                    R.raw.wall_eng_fourtyfive,R.raw.wall_eng_fourtysix,R.raw.wall_eng_fourtyseven_two_colonel_chief,
                    R.raw.wall_eng_fourtyeight_two_honorary_colonels,R.raw.wall_eng_fourtynine_the_colonel,R.raw.wall_eng_fifty_army_commandants,
                    R.raw.wall_eng_fiftyone_centre_commandants,

                    //wall fifty two not available
                    R.raw.wall_eng_fiftyone_centre_commandants,

                    R.raw.wall_eng_fiftythree_two_sportstwo,
                    R.raw.wall_eng_fiftyfour_two_adventure,R.raw.wall_eng_fiftyfive_rdp,R.raw.wall_eng_fiftysix,
                    R.raw.wall_eng_fiftyseven_two_un_missions,R.raw.wall_eng_fiftyeight,R.raw.wall_eng_fiftynine,
                    R.raw.wall_eng_sixty,R.raw.wall_eng_sixtyone,R.raw.wall_eng_sixtytwo,
                    R.raw.wall_eng_sixtythree,R.raw.wall_eng_sixtyfour_chakras,R.raw.wall_eng_sixtyfive_mangalore_battle,
                    R.raw.wall_eng_sixtysix,R.raw.wall_eng_sixtyseven_nineteenseventyone_one,R.raw.wall_eng_sixtyeight_two_nineteenseventyone_two,
                    R.raw.wall_eng_sixtynine_two_nineteenseventyone_three,R.raw.wall_eng_seventy_two_nineteenseventyone_four,R.raw.wall_eng_seventyone_nineteenseventyone_five,
                    R.raw.wall_eng_seventytwo_two_twentyone_rr,R.raw.wall_eng_seventythree_two_ciops,R.raw.wall_eng_seventyfour,
                    R.raw.wall_eng_seventyfive,R.raw.wall_eng_seventysix_seventyseven_missile,R.raw.wall_eng_seventyeight_eightyone_bmp,R.raw.wall_eng_eighttwo_t_fiftyfive_tank


            };
            final int[] audio_hindi={
                    R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
                    R.raw.wall_hindi_one,
            };

            final int[] slider_default={R.drawable.noimage};
            final int[] slider_zero={R.drawable.wall_one_one,R.drawable.wall_one_two,R.drawable.wall_one_three,R.drawable.wall_one_four,R.drawable.wall_one_five,R.drawable.wall_one_six,R.drawable.wall_one_seven,R.drawable.wall_one_eight,R.drawable.wall_one_nine,R.drawable.wall_one_ten,R.drawable.wall_one_eleven};
            final int[] slider_one={R.drawable.noimage};
            final int[] slider_two={R.drawable.two_one,R.drawable.two_two,R.drawable.two_three,R.drawable.two_four,R.drawable.two_five,R.drawable.two_six,R.drawable.two_seven,R.drawable.two_eight,R.drawable.two_nine};
            final int[] slider_three={R.drawable.three_one,R.drawable.three_two,R.drawable.three_three,R.drawable.three_four,R.drawable.three_five,R.drawable.three_six,R.drawable.three_seven,R.drawable.three_eight,R.drawable.three_nine,R.drawable.three_ten,R.drawable.three_eleven};
            final int[] slider_four={R.drawable.four_one,R.drawable.four_two,R.drawable.four_three,R.drawable.four_four,R.drawable.four_five,R.drawable.four_six,R.drawable.four_seven,R.drawable.four_eight,R.drawable.four_nine,R.drawable.four_ten};
            final int[] slider_five={R.drawable.wall_five_one_two,R.drawable.wall_five_two_two,R.drawable.wall_five_three,R.drawable.wall_five_four_two};
            final int[] slider_six={R.drawable.wall_six_one,R.drawable.wall_six_two,R.drawable.wall_six_three,R.drawable.wall_six_four};
            final int[] slider_seven={R.drawable.wall_seven_one_two,R.drawable.wall_seven_two_two,R.drawable.wall_seven_three_two,R.drawable.wall_seven_four,R.drawable.wall_seven_five,R.drawable.wall_seven_eleven};
            final int[] slider_eight={R.drawable.wall_eight_one};
            final int[] slider_nine={R.drawable.wall_nine_one};
            final int[] slider_ten={R.drawable.wall_ten_two,R.drawable.wall_ten_three};

            final int[] slider_eleven={R.drawable.wall_eleven_two,R.drawable.wall_eleven_three};
            final int[] slider_twelve={R.drawable.noimage,R.drawable.noimage,R.drawable.noimage};
            final int[] slider_thirteen={R.drawable.wall_thirteen_two};
            final int[] slider_fourteen={R.drawable.wall_fourteen_one_two,R.drawable.wall_fourteen_two,R.drawable.wall_fourteen_four};
            final int[] slider_fifthteen={R.drawable.wall_fifteen_one,R.drawable.wall_sixteen_one};
//            final int[] slider_sixteen={R.drawable.wall_sixteen_one};
            final int[] slider_seventeen={R.drawable.wall_seventeen_one};
            final int[] slider_eighteen={R.drawable.wall_eighteen_one};
            final int[] slider_nineteen={R.drawable.wall_ninteen_one};
            final int[] slider_twenty={R.drawable.wall_twenty_point_one,R.drawable.wall_twenty_point_two,R.drawable.wall_twenty_point_three};

            final int[] slider_twentyone={R.drawable.wall_twentyone_one};
            final int[] slider_twentytwo={R.drawable.wall_twentytwo_one};
            final int[] slider_twentythree={R.drawable.wall_twentythree_one,R.drawable.wall_twentyfour_one,
                    R.drawable.wall_twentyfive,R.drawable.wall_twentysix_one,R.drawable.wall_twentyseven_one,
                    R.drawable.wall_twentyeight_one,R.drawable.wall_twentynine_one,
                    R.drawable.wall_thirty_point_one,R.drawable.wall_thirty_point_two,R.drawable.wall_thirty_point_three
            };
//            final int[] slider_twentyfour={R.drawable.wall_twentyfour_one};
//            final int[] slider_twentyfive={R.drawable.wall_twentyfive};
//            final int[] slider_twentysix={R.drawable.wall_twentysix_one};
//            final int[] slider_twentyseven={R.drawable.wall_twentyseven_one};
//            final int[] slider_twentyeight={R.drawable.wall_twentyeight_one};
//            final int[] slider_twentynine={R.drawable.wall_twentynine_one};
            final int[] slider_thirty={R.drawable.wall_thirty_point_one,R.drawable.wall_thirty_point_two,R.drawable.wall_thirty_point_three};

            final int[] slider_thirtyone={R.drawable.wall_thirtyone_one_two,R.drawable.wall_thirtyone_two,R.drawable.wall_thirtyone_three,R.drawable.wall_thirtyone_four};
            final int[] slider_thirtytwo={R.drawable.wall_thirtytwo_one_two,R.drawable.wall_thirtytwo_two};
            final int[] slider_thirtythree={R.drawable.wall_thirtythree_one_two,R.drawable.wall_thirtythree_two};
            final int[] slider_thirtyfour={R.drawable.wall_thirtyfour_one,R.drawable.wall_thirtyfour_two};
            final int[] slider_thirtyfive={R.drawable.wall_thirtyfive_one,R.drawable.wall_thirtyfive_two};
            final int[] slider_thirtysix={R.drawable.wall_thirtysix_one,R.drawable.wall_thirtysix_two};
            final int[] slider_thirtyseven={R.drawable.wall_thirtyseven_two};
            final int[] slider_thirtyeight={R.drawable.wall_thirtyeight_one,R.drawable.wall_thirtyeight_two};
            final int[] slider_thirtynine={R.drawable.wall_thirtynine_one,R.drawable.wall_thirtynine_two,R.drawable.wall_thirtynine_three,R.drawable.wall_thirtynine_four};
            final int[] slider_fourty={R.drawable.wall_fourty_point_one,R.drawable.wall_fourty_point_two};

            final int[] slider_fourtyone={R.drawable.wall_fourtyone_one,R.drawable.wall_fourtyone_two};
            final int[] slider_fourtytwo={R.drawable.noimage,R.drawable.noimage,R.drawable.noimage};
            final int[] slider_fourtythree={R.drawable.wall_fourtythree_one,R.drawable.wall_fourtythree_two,R.drawable.wall_fourtythree_four,R.drawable.wall_fourtythree_five};
            final int[] slider_fourtyfour={R.drawable.wall_fourtyfour_one};
            final int[] slider_fourtyfive={R.drawable.wall_fourtyfive_one,R.drawable.wall_fourtyfive_two};
            final int[] slider_fourtysix={R.drawable.wall_fourtysix_one,R.drawable.wall_fourtysix_two,R.drawable.wall_fourtysix_four,R.drawable.wall_fourtysix_five,R.drawable.wall_fourtysix_seven};
            final int[] slider_fourtyseven={R.drawable.wall_fourtyseven_one};
            final int[] slider_fourtyeight={R.drawable.wall_fourtyeight_one,R.drawable.wall_fourtyeight_two};
            final int[] slider_fourtynine={R.drawable.wall_fourtynine_one,R.drawable.wall_fourtynine_two,R.drawable.wall_fourtynine_three,R.drawable.wall_fourtynine_four,R.drawable.wall_fourtynine_five};


            tv_android = (TextView)view.findViewById(R.id.tv_android);
            img_android = (FrameLayout) view.findViewById(R.id.img_android);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() == 0) {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 0);
                        environmentPage.putExtra("sliders",slider_zero);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    //archive
                    else if (getAdapterPosition() == 1){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 1);
                        environmentPage.putExtra("sliders",slider_one);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 2){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_2","wall_2");
                        environmentPage.putExtra("position", 2);
                        environmentPage.putExtra("sliders",slider_two);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 3){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_three);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("wall_3","wall_3");
                        environmentPage.putExtra("position", 3);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 4){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_4","wall_4");
                        environmentPage.putExtra("position", 4);
                        environmentPage.putExtra("sliders",slider_four);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 5){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                    environmentPage.putExtra("sliders",slider_five);
                        environmentPage.putExtra("position", 5);
                        environmentPage.putExtra("wall_5","wall_5");
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 6){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_6","wall_6");
                        environmentPage.putExtra("position", 6);
                    environmentPage.putExtra("sliders",slider_six);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 7){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_7","wall_7");
                        environmentPage.putExtra("position", 7);
                    environmentPage.putExtra("sliders",slider_seven);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 8){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_8","wall_8");
                        environmentPage.putExtra("position", 8);
                    environmentPage.putExtra("sliders",slider_eight);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 9){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_9","wall_9");
                        environmentPage.putExtra("position", 9);
                    environmentPage.putExtra("sliders",slider_nine);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 10){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_10","wall_10");
                        environmentPage.putExtra("position", 10);
                        environmentPage.putExtra("sliders",slider_ten);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 11){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_11","wall_11");
                        environmentPage.putExtra("position", 11);
                        environmentPage.putExtra("sliders",slider_eleven);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 12){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_12","wall_12");
                        environmentPage.putExtra("position", 12);
                        environmentPage.putExtra("sliders",slider_twelve);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 13){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_13","wall_13");
                        environmentPage.putExtra("position", 13);
                        environmentPage.putExtra("sliders",slider_thirteen);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 14){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_14","wall_14");
                        environmentPage.putExtra("position", 14);
                        environmentPage.putExtra("sliders",slider_fourteen);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 15){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_15","wall_15");
                        environmentPage.putExtra("position", 15);
                        environmentPage.putExtra("sliders",slider_fifthteen);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 16){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_17","wall_17");
                        environmentPage.putExtra("position", 16);
                        environmentPage.putExtra("sliders",slider_seventeen);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 17){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_18","wall_18");
                        environmentPage.putExtra("position", 17);
                        environmentPage.putExtra("sliders",slider_eighteen);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 18){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_19","wall_19");
                        environmentPage.putExtra("position", 18);
                        environmentPage.putExtra("sliders",slider_nineteen);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 19){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_20","wall_20");
                        environmentPage.putExtra("position", 19);
                        environmentPage.putExtra("sliders",slider_twenty);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 20){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_21","wall_21");
                        environmentPage.putExtra("position", 20);
                        environmentPage.putExtra("sliders",slider_twentyone);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 21){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_22","wall_22");
                        environmentPage.putExtra("position", 21);
                        environmentPage.putExtra("sliders",slider_twentytwo);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 22){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_23","wall_23");
                        environmentPage.putExtra("position", 22);
                        environmentPage.putExtra("sliders",slider_twentythree);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 23){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_31","wall_31");
                        environmentPage.putExtra("position", 23);
                        environmentPage.putExtra("sliders",slider_thirtyone);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 24){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_25","wall_25");
                        environmentPage.putExtra("wall_32","wall_32");
                        environmentPage.putExtra("position", 24);
                        environmentPage.putExtra("sliders",slider_thirtytwo);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 25){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_33","wall_33");
                        environmentPage.putExtra("position", 25);
                        environmentPage.putExtra("sliders",slider_thirtythree);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 26){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_34","wall_34");
                        environmentPage.putExtra("position", 26);
                        environmentPage.putExtra("sliders",slider_thirtyfour);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 27){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_35","wall_35");
                        environmentPage.putExtra("position", 27);
                        environmentPage.putExtra("sliders",slider_thirtyfive);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 28){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_36","wall_36");
                        environmentPage.putExtra("position", 28);
                        environmentPage.putExtra("sliders",slider_thirtysix);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 29){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_37","wall_37");
                        environmentPage.putExtra("position", 29);
                        environmentPage.putExtra("sliders",slider_thirtyseven);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 30){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_38","wall_38");
                        environmentPage.putExtra("position", 30);
                        environmentPage.putExtra("sliders",slider_thirtyeight);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 31){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_39","wall_39");
                        environmentPage.putExtra("position", 31);
                        environmentPage.putExtra("sliders",slider_thirtynine);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 32){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_40","wall_40");
                        environmentPage.putExtra("position", 32);
                        environmentPage.putExtra("sliders",slider_fourty);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 33){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_41","wall_41");
                        environmentPage.putExtra("position", 33);
                        environmentPage.putExtra("sliders",slider_fourtyone);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 34){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_42","wall_42");
                        environmentPage.putExtra("position", 34);
                        environmentPage.putExtra("sliders",slider_fourtytwo);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 35){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_43","wall_43");
                        environmentPage.putExtra("sliders",slider_fourtythree);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 35);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 36){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_44","wall_44");
                        environmentPage.putExtra("position", 36);
                        environmentPage.putExtra("sliders",slider_fourtyfour);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 37){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_45","wall_45");
                        environmentPage.putExtra("position", 37);
                        environmentPage.putExtra("sliders",slider_fourtyfive);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 38){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_46","wall_46");
                        environmentPage.putExtra("position", 38);
                        environmentPage.putExtra("sliders",slider_fourtysix);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 39){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_47","wall_47");
                        environmentPage.putExtra("position", 39);
                        environmentPage.putExtra("sliders",slider_fourtyseven);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 40){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_48","wall_48");
                        environmentPage.putExtra("position", 40);
                        environmentPage.putExtra("sliders",slider_fourtyeight);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 41){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_49","wall_49");
                        environmentPage.putExtra("position", 41);
                        environmentPage.putExtra("sliders",slider_fourtynine);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 42){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 42);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 43){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 43);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 44){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 44);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 45){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 45);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 46){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 46);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 47){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 47);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 48){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 48);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 49){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 49);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 50){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 50);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 51){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 51);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 52){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("position", 52);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 53){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 53);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 54){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 54);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 55){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 55);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 56){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 56);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 57){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 57);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 58){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 58);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 59){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", 59);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);

                    }else if (getAdapterPosition() == 60){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 60);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 61){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 61);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }

                    else if (getAdapterPosition() == 62){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 62);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 63){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 63);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 64){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 64);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 65){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 65);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 66){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 66);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 67){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 67);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 68){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 68);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 69){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 69);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 70){
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 70);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                    else if (getAdapterPosition() == 71){

                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("sliders",slider_default);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.putExtra("position", 71);
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }























//
                    else {
                        Intent environmentPage = new Intent(context, WallExplanation.class);
                        environmentPage.putExtra("audio_english", audio_eng[getAdapterPosition()]);
                        environmentPage.putExtra("audio_hindi", audio_hindi[getAdapterPosition()]);
                        environmentPage.putExtra("image", walls.get(getAdapterPosition()).getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
//                    environmentPage.putExtra("sliders",walls.get(getAdapterPosition()).getSliders());
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(environmentPage);
                    }
                }
            });
        }
    }

}
