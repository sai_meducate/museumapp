package museum.m_educate.com.museum;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by DELL on 11/7/2017.
 */

public class ColonelsCentreCommandants extends AppCompatActivity {

    Button colonel_chief,honorary_colonel,colonel_of_the_regiment,army_commandant,centre_commandant,home,back,exit;
    private String audio_in = "Hindi";


    final int[] slider_fourtyseven={R.drawable.wall_fourtyseven_one};
    final int[] slider_fourtyeight={R.drawable.wall_fourtyeight_one,R.drawable.wall_fourtyeight_two};
    final int[] slider_fourtynine={R.drawable.wall_fourtynine_one,R.drawable.wall_fourtynine_two,R.drawable.wall_fourtynine_three,R.drawable.wall_fourtynine_four,R.drawable.wall_fourtynine_five};


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.colcnelscentercommandants);

        colonel_chief = (Button) findViewById(R.id.colonel_chief);
        honorary_colonel = (Button) findViewById(R.id.honorary_colonel);
        colonel_of_the_regiment = (Button) findViewById(R.id.colonel_of_the_regiment);
        army_commandant = (Button) findViewById(R.id.army_commandant);
        centre_commandant= (Button) findViewById(R.id.centre_commandant);
        back = (Button) findViewById(R.id.back_button);
        home = (Button) findViewById(R.id.cont);
        exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(ColonelsCentreCommandants.this);

                alert.setTitle("Are you sure you want to close this tour ?");

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }


                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });

        try {
            audio_in = getIntent().getStringExtra("audio_in");

        } catch (Exception e) {
            Toast.makeText(this, "Audio not selected", Toast.LENGTH_LONG).show();
        }

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(ColonelsCentreCommandants.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(ColonelsCentreCommandants.this, GroupTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });

        colonel_chief.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                wall8
                Intent environmentPage = new Intent(ColonelsCentreCommandants.this, WallExplanation.class);
//               pass to wall 47 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_fourtyseven_two_colonel_chief);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_fourtyseven_two_colonel_chief);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("sliders",slider_fourtyseven);
                environmentPage.putExtra("position", 46);

                environmentPage.putExtra("from","colonels");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });

        honorary_colonel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                pass to wall 48 raw file data
                Intent environmentPage = new Intent(ColonelsCentreCommandants.this, WallExplanation.class);
                environmentPage.putExtra("audio_english",R.raw.wall_eng_fourtyeight_two_honorary_colonels);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_fourtyeight_two_honorary_colonels);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("sliders",slider_fourtyeight);
                environmentPage.putExtra("from","colonels");
                environmentPage.putExtra("position", 47);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
//wall9
            }
        });

        colonel_of_the_regiment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(ColonelsCentreCommandants.this, WallExplanation.class);
//               pass to wall 49 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_fourtynine_the_colonel);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_fourtynine_the_colonel);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("sliders",slider_fourtynine);
                environmentPage.putExtra("from","colonels");
                environmentPage.putExtra("position", 48);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();

            }
        });
        army_commandant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(ColonelsCentreCommandants.this, WallExplanation.class);
//               pass to wall 50 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_fifty_army_commandants);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_fifty_army_commandants);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("from","colonels");
                environmentPage.putExtra("position", 49);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
        centre_commandant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(ColonelsCentreCommandants.this, WallExplanation.class);
//               pass to wall 51 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_fiftyone_centre_commandants);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_fiftyone_centre_commandants);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("from","colonels");
                environmentPage.putExtra("position", 40);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent environmentPage = new Intent(ColonelsCentreCommandants.this, GroupTourActivity.class);
        environmentPage.putExtra("audio_in", audio_in);
        startActivity(environmentPage);
        finish();
    }
}
