package museum.m_educate.com.museum;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by DELL on 11/6/2017.
 */

public class EarlyGuardUnits extends AppCompatActivity {
    Button firstguard, secondguard, thirdguard, fourguard,capturedweapons,heritageweapons,home,back,exit;
    private String audio_in = "Hindi";


    final int[] slider_eight={R.drawable.wall_eight_one};
    final int[] slider_nine={R.drawable.wall_nine_one};
    final int[] slider_ten={R.drawable.wall_ten_two,R.drawable.wall_ten_three};

    final int[] slider_eleven={R.drawable.wall_eleven_two,R.drawable.wall_eleven_three};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.earlyguardunits);

        firstguard = (Button) findViewById(R.id.firstguard);
        secondguard = (Button) findViewById(R.id.secondguard);
        thirdguard = (Button) findViewById(R.id.thirdguard);
        fourguard = (Button) findViewById(R.id.fourguard);
        capturedweapons= (Button) findViewById(R.id.capturedweapon);
        heritageweapons = (Button) findViewById(R.id.heritageweapon);

        back = (Button) findViewById(R.id.back_button);
        home = (Button) findViewById(R.id.cont);
        exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(EarlyGuardUnits.this);

                alert.setTitle("Are you sure you want to close this tour ?");

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }


                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(EarlyGuardUnits.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(EarlyGuardUnits.this, GroupTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        try {
            audio_in = getIntent().getStringExtra("audio_in");

        } catch (Exception e) {
            Toast.makeText(this, "Audio not selected", Toast.LENGTH_LONG).show();
        }
        firstguard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                wall8
                Intent environmentPage = new Intent(EarlyGuardUnits.this, WallExplanation.class);
//               pass to wall8 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_eight);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_eight);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("sliders",slider_eight);
                environmentPage.putExtra("position", 7);
                environmentPage.putExtra("from","earlyguradunits");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });

        secondguard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                pass to wall9 raw file data
                Intent environmentPage = new Intent(EarlyGuardUnits.this, WallExplanation.class);
                environmentPage.putExtra("audio_english",R.raw.wall_eng_nine);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_nine);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("sliders",slider_nine);
                environmentPage.putExtra("position", 8);

                environmentPage.putExtra("from","earlyguradunits");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
//wall9
            }
        });

        thirdguard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(EarlyGuardUnits.this, WallExplanation.class);
//               pass to wall10 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_ten);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_ten);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("sliders",slider_ten);
                environmentPage.putExtra("position", 9);

                environmentPage.putExtra("from","earlyguradunits");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();

            }
        });
        fourguard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(EarlyGuardUnits.this, WallExplanation.class);
//               pass to wall10 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_eleven);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_eleven);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("sliders",slider_eleven);
                environmentPage.putExtra("position", 10);

                environmentPage.putExtra("from","earlyguradunits");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
        capturedweapons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(EarlyGuardUnits.this, WallExplanation.class);
//               pass to wall8 raw file data
                environmentPage.putExtra("audio_english",R.raw.early_guard_capturedweapons);
                environmentPage.putExtra("audio_hindi",R.raw.early_guard_capturedweapons);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("position", 1);

                environmentPage.putExtra("from","earlyguradunits");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
        heritageweapons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(EarlyGuardUnits.this, WallExplanation.class);
//               pass to wall8 raw file data
                environmentPage.putExtra("audio_english",R.raw.early_guard_heritage_weapon_up);
                environmentPage.putExtra("audio_hindi",R.raw.early_guard_heritage_weapon_up);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("from","earlyguradunits");
                environmentPage.putExtra("position", 4);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });


    }

    @Override
    public void onBackPressed() {
        Intent environmentPage = new Intent(EarlyGuardUnits.this, GroupTourActivity.class);
        environmentPage.putExtra("audio_in", audio_in);
        startActivity(environmentPage);
        finish();
    }
}
