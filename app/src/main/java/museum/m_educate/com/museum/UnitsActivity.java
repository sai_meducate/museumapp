package museum.m_educate.com.museum;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by macbookpro on 10/10/17.
 */

public class UnitsActivity extends AppCompatActivity {

        private final String wall_names[] = {

                " GRC ",
                " 1 GUARDS",
                " 2 GUARDS",
                " 3 GUARDS",
                " 4 GUARDS",
                " 5 GUARDS",
                " 6 GUARDS",
                " 7 GUARDS",
                " 8 GUARDS",
                " 9 GUARDS",
                " 10 GUARDS",
                " 11 GUARDS",
                " 12 GUARDS",
                " 13 GUARDS",
                " 14 GUARDS",
                " 15 GUARDS",
                " 16 GUARDS",
                " 17 GUARDS",
                " 18 GUARDS",
                " 19 GUARDS",
                " 20 GUARDS",
                " 22 GUARDS",
                " 117 TA",
                " 125 TA",
                " 21 RR"

        };

        private final int wall_image_urls[] = {
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
                R.drawable.no_preview,
        };
    private String audio_in="Hindi";
    Button back, home,exit;

    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.sequencial_tour);
            try {
                audio_in = getIntent().getStringExtra("audio_in");

            } catch (Exception e) {
                Toast.makeText(this, "Audio not selected", Toast.LENGTH_LONG).show();
            }
        initViews();
        back = (Button) findViewById(R.id.back_button);
        home = (Button) findViewById(R.id.cont);
        exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(UnitsActivity.this);

                alert.setTitle("Are you sure you want to close this tour ?");

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }


                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(UnitsActivity.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(UnitsActivity.this, GroupTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
    }
    private void initViews(){
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),3);
        recyclerView.setLayoutManager(layoutManager);

        ArrayList<Units> walls = prepareData();
        UnitsDataAdapter adapter = new UnitsDataAdapter(getApplicationContext(),walls,audio_in);
        recyclerView.setAdapter(adapter);

    }
    private ArrayList<Units> prepareData(){

        ArrayList<Units> wall = new ArrayList<>();
        for(int i=0;i<wall_names.length;i++){
            Units walls = new Units();
            walls.setWall(wall_names[i]);
            walls.setImage_url(wall_image_urls[i]);
            wall.add(walls);
        }
        return wall;
    }

    @Override
    public void onBackPressed() {
        Intent environmentPage = new Intent(this, GroupTourActivity.class);
        environmentPage.putExtra("audio_in", audio_in);
        startActivity(environmentPage);
        finish();
    }
}

