package museum.m_educate.com.museum;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class GroupTourActivity extends AppCompatActivity {
    Button  founder,
            units,
            early_guard,
            award_winners,
            investiture,
            memories,
            colonels_centre_commandants,
            sports_and_adventure_activities,
            rdp,
            un_missions,
            battle_honours,
            ci_ops,
            cut_models,
            back,
            home,exit;

    final int[] default_image ={R.drawable.noimage,R.drawable.noimage,R.drawable.noimage};
    final int[] slider_two={R.drawable.wall_two_one,R.drawable.wall_two_two,R.drawable.wall_two_three};
    final int[] slider_twentythree={R.drawable.wall_twentythree_one,R.drawable.wall_twentyfour_one,R.drawable.wall_twentyfive,R.drawable.wall_twentysix_one
            ,R.drawable.wall_twentyseven_one,R.drawable.wall_twentyeight_one,R.drawable.wall_twentynine_one,
            R.drawable.wall_thirty_point_one,R.drawable.wall_thirty_point_two,R.drawable.wall_thirty_point_three};
    private String audio_in="Hindi";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.group_tour);

        units = (Button) findViewById(R.id.units);
        founder = (Button) findViewById(R.id.founder);
        early_guard = (Button) findViewById(R.id.early_guard);
        award_winners = (Button) findViewById(R.id.award_winners);
        investiture = (Button) findViewById(R.id.investiture);
        memories = (Button) findViewById(R.id.memories);
        colonels_centre_commandants = (Button) findViewById(R.id.colonels_centre_commandants);
        sports_and_adventure_activities = (Button) findViewById(R.id.sports_and_adventure_activities);
        rdp = (Button) findViewById(R.id.rdp);
        un_missions = (Button) findViewById(R.id.un_missions);
        battle_honours = (Button) findViewById(R.id.battle_honours);
        ci_ops = (Button) findViewById(R.id.ci_ops);
        cut_models = (Button) findViewById(R.id.cut_models);

        back = (Button) findViewById(R.id.back_button);
        home = (Button) findViewById(R.id.cont);

        exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(GroupTourActivity.this);

                alert.setTitle("Are you sure you want to close this tour ?");

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }


                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });

        try {
            audio_in = getIntent().getStringExtra("audio_in");

        } catch (Exception e) {
            Toast.makeText(this, "Audio not selected", Toast.LENGTH_LONG).show();
        }
        units.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, UnitsActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });

        founder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, WallExplanation.class);
//               pass to wall 33 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_two);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_two);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("sliders",slider_two);
                environmentPage.putExtra("position", 0);

                environmentPage.putExtra("from","founder");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
        early_guard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, EarlyGuardUnits.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        award_winners.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, AwardWinners.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        investiture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, WallExplanation.class);
//               pass to wall 23 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_twentythree_thirty_honorary_colonel);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_twentythree_thirty_honorary_colonel);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("sliders",slider_twentythree);
                environmentPage.putExtra("position", 22);

                environmentPage.putExtra("from","grouptouractivity");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });

        memories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, MemoriesSliders.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });

        colonels_centre_commandants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, ColonelsCentreCommandants.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        sports_and_adventure_activities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, SportAdventureActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });

        rdp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, WallExplanation.class);
//               pass to wall 55 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_fiftyfive_rdp);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_fiftyfive_rdp);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("position", 54);

                environmentPage.putExtra("from","grouptouractivity");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
        un_missions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, WallExplanation.class);
//               pass to wall 57 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_fiftyseven_two_un_missions);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_fiftyseven_two_un_missions);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("position", 56);

                environmentPage.putExtra("from","grouptouractivity");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
        battle_honours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, BattleHonours.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        ci_ops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent environmentPage = new Intent(GroupTourActivity.this, CiOps.class);
//                environmentPage.putExtra("audio_in", audio_in);
//                startActivity(environmentPage);
//                finish();
                Intent environmentPage = new Intent(GroupTourActivity.this, WallExplanation.class);
//               pass to wall 33 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_seventythree_two_ciops);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_seventythree_two_ciops);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("sliders",default_image);
                environmentPage.putExtra("position", 0);

                environmentPage.putExtra("from","grouptouractivity");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });

        cut_models.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(GroupTourActivity.this, CutModels.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });



    }

    @Override
    public void onBackPressed() {
        Intent environmentPage = new Intent(GroupTourActivity.this, GoroupSequenceTourActivity.class);
        environmentPage.putExtra("audio_in", audio_in);
        startActivity(environmentPage);
        finish();
    }
}