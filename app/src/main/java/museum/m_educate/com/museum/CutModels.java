package museum.m_educate.com.museum;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by DELL on 11/7/2017.
 */

public class CutModels extends AppCompatActivity {
    Button missile,bmp,ttank,home,back,exit;
    private String audio_in = "Hindi";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.cutmodels);

        missile = (Button) findViewById(R.id.missile);
        bmp = (Button) findViewById(R.id.bmp);
        ttank = (Button) findViewById(R.id.ttank);
        back = (Button) findViewById(R.id.back_button);
        home = (Button) findViewById(R.id.cont);
        exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(CutModels.this);

                alert.setTitle("Are you sure you want to close this tour ?");

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }


                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(CutModels.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(CutModels.this, GroupTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });



        try {
            audio_in = getIntent().getStringExtra("audio_in");

        } catch (Exception e) {
            Toast.makeText(this, "Audio not selected", Toast.LENGTH_LONG).show();
        }
        missile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                wall8
                Intent environmentPage = new Intent(CutModels.this, WallExplanation.class);
//               pass to wall 52 and 53 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_seventysix_seventyseven_missile);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_seventysix_seventyseven_missile);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("from","cutmodels");
                environmentPage.putExtra("position", 65);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });

        bmp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                pass to wall 68,69,70,71 raw file data
                Intent environmentPage = new Intent(CutModels.this, WallExplanation.class);
                environmentPage.putExtra("audio_english",R.raw.wall_eng_seventyeight_eightyone_bmp);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_seventyeight_eightyone_bmp);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("position", 77);
                environmentPage.putExtra("from","cutmodels");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
        ttank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                no action
                Intent environmentPage = new Intent(CutModels.this, WallExplanation.class);
                environmentPage.putExtra("audio_english",R.raw.wall_eng_eighttwo_t_fiftyfive_tank);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_eighttwo_t_fiftyfive_tank);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("position", 81);

                environmentPage.putExtra("from","cutmodels");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent environmentPage = new Intent(CutModels.this, GroupTourActivity.class);
        environmentPage.putExtra("audio_in", audio_in);
        startActivity(environmentPage);
        finish();
    }
}
