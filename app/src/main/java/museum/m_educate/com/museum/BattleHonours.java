package museum.m_educate.com.museum;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by DELL on 11/7/2017.
 */

public class BattleHonours extends AppCompatActivity {
    Button nineteensixtyfive,nineteenseventyone,home,back,exit;
    private String audio_in = "Hindi";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.battlehonours);

        nineteensixtyfive = (Button) findViewById(R.id.nineteensixtyfive);
        nineteenseventyone = (Button) findViewById(R.id.nineteenseventyone);
        back = (Button) findViewById(R.id.back_button);
        home = (Button) findViewById(R.id.cont);
        exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(BattleHonours.this);

                alert.setTitle("Are you sure you want to close this tour ?");

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }


                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });


        try {
            audio_in = getIntent().getStringExtra("audio_in");

        } catch (Exception e) {
            Toast.makeText(this, "Audio not selected", Toast.LENGTH_LONG).show();
        }

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(BattleHonours.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(BattleHonours.this, GroupTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        nineteensixtyfive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                wall8
                Intent environmentPage = new Intent(BattleHonours.this, WallExplanation.class);
//               pass to wall 52 and 53 raw file data
                environmentPage.putExtra("audio_english",R.raw.wall_eng_sixtysix);
                environmentPage.putExtra("audio_hindi",R.raw.wall_eng_sixtysix);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("position", 67);

                environmentPage.putExtra("from","battlehonours");
                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });

        nineteenseventyone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                pass to wall 68,69,70,71 raw file data
                Intent environmentPage = new Intent(BattleHonours.this, WallExplanation.class);
                environmentPage.putExtra("audio_english",R.raw.battle_honour_nineteenseventyone);
                environmentPage.putExtra("audio_hindi",R.raw.battle_honour_nineteenseventyone);
                environmentPage.putExtra("image",R.drawable.no_preview);
                environmentPage.putExtra("audio_in",audio_in);
                environmentPage.putExtra("from","battlehonours");
                environmentPage.putExtra("position", 60);

                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(environmentPage);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent environmentPage = new Intent(BattleHonours.this, GroupTourActivity.class);
        environmentPage.putExtra("audio_in", audio_in);
        startActivity(environmentPage);
        finish();
    }
}
