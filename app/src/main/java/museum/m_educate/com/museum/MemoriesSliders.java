package museum.m_educate.com.museum;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by DELL on 11/8/2017.
 */

public class MemoriesSliders extends AppCompatActivity {

    Button buttoneng, buttonhindi, buttonpause, btnNext;
    ViewPager viewpager;
    private Button buttonpriv;
    Button button_language, button_play, button_pause, button_stop,
            button_continue, button_back,home,exit;
    SeekBar seekbar;
    TextView totaltime, ongoingtime;
    String audio_in = "Hindi";
    Handler seekHandler = new Handler();
    MediaPlayer player;
    private Handler mHandler = new Handler();
    private Timer timer;
    int xaa = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.memoriessliders);

        home = (Button) findViewById(R.id.cont);
//        button_language = (Button) findViewById(R.id.language);
        button_back = (Button) findViewById(R.id.back_button);
        button_play = (Button) findViewById(R.id.paly);
        button_play.setText("Play");
        button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_play_arrow_black_24dp, 0, 0);
        button_stop = (Button) findViewById(R.id.stop);
        totaltime = (TextView) findViewById(R.id.total_time);
        ongoingtime = (TextView) findViewById(R.id.ongoing_time);
        seekbar = (SeekBar) findViewById(R.id.seekbar);

        exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(MemoriesSliders.this);

                alert.setTitle("Are you sure you want to close this tour ?");

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }


                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });
        try {
            audio_in = getIntent().getStringExtra("audio_in");
            if(audio_in.equalsIgnoreCase("English")){
                button_language.setText(getApplicationContext().getString(R.string.hindi_language));
            }else if(audio_in.equalsIgnoreCase("Hindi")){
                button_language.setText("Listen in English");
            }
        } catch (Exception e) {
            //Toast.makeText(this, "Audio not selected", Toast.LENGTH_LONG).show();
        }
//        startAudio(xaa);

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MemoriesSliders.SliderTimer(), 4000, 6000);

        int[] mResources = {
                R.drawable.sld_one,
                R.drawable.sld_two,
                R.drawable.sld_three,
        };


        MyCustomPagerAdapter myCustomPagerAdapter;


        viewpager = (ViewPager) findViewById(R.id.viewpager);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);

        myCustomPagerAdapter = new MyCustomPagerAdapter(MemoriesSliders.this, mResources);
        viewpager.setAdapter(myCustomPagerAdapter);
        indicator.setViewPager(viewpager);

        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stop();
                Intent indonesianActivty = new Intent(MemoriesSliders.this, GroupTourActivity.class);
                indonesianActivty.putExtra("audio_in", audio_in);
                startActivity(indonesianActivty);
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stop();
                Intent environmentPage = new Intent(MemoriesSliders.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        button_play.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean b = true;
                if (button_play.getText().equals("Play") && b == true) {
                    startAudio(0);
                    button_play.setText("Pause");
                    button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_pause_black_24dp, 0, 0);

                    b = false;
                } else if (button_play.getText().equals("Pause")) {
                    xaa = player.getCurrentPosition();
                    player.pause();
                    button_play.setText("Resume");
                    button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_play_arrow_black_24dp, 0, 0);

                    b = false;
                } else if (button_play.getText().equals("Resume") && b == true) {
                    player.seekTo(xaa);
                    startAudio(xaa);
                    button_play.setText("Pause");
                    button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_pause_black_24dp, 0, 0);

                    b = false;
                }


            }

        });


        button_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button_play.setText("Play");
                button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_play_arrow_black_24dp, 0, 0);
                seekbar.setProgress(0);
                stop();
            }
        });

//        button_back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                stop();
//                Intent environmentPage = new Intent(MemoriesSliders.this, MainActivity.class);
//                environmentPage.putExtra("audio_in", audio_in);
//                startActivity(environmentPage);
//                finish();
//            }
//        });

//        button_language.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("audio", "onClick: "+button_language.getText());
//                if (button_language.getText().equals("Language")) {
//                    showLanguge();
//                } else if (button_language.getText().equals("Listen in English")) {
//                    stop();
//                    audio_in = "English";
//                    startAudio(xaa);
//                    button_language.setText(getApplicationContext().getString(R.string.hindi_language));
//
//                } else if (button_language.getText().equals("हिंदी में सुनने")) {
//                    stop();
//                    audio_in = "Hindi";
//                    startAudio(xaa);
//                    button_language.setText("Listen in English");
//                }
//            }
//        });



    }

    private void showLanguge() {
        final CharSequence[] photo = {"English", "Hindi"};

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Select Language");

        alert.setSingleChoiceItems(photo, -1, new

                DialogInterface.OnClickListener()

                {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (photo[which] == "English")

                        {
                            audio_in = "English";
                            button_language.setText(getApplicationContext().getString(R.string.hindi_language));
                            xaa = 0;
                            stop();
                            startAudio(xaa);
                            dialog.dismiss();

                        } else if (photo[which] == "Hindi")

                        {

                            audio_in = "Hindi";
                            button_language.setText("Listen in English");
                            xaa = 0;
                            stop();
                            startAudio(xaa);
                            dialog.dismiss();

                        }
                    }

                });
        alert.show();
    }

    private void startAudio(int i) {
        if (audio_in.equalsIgnoreCase("Hindi")) {
            player = MediaPlayer.create(this, R.raw.slider_hindi);
            player.seekTo(i);
        } else {
            player = MediaPlayer.create(this, R.raw.sliderscreen_english);
            player.seekTo(i);
        }
        button_play.setText("Pause");
        button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_pause_black_24dp, 0, 0);

        player.start();
//        player = MediaPlayer.create(this, R.raw.splash_weclcome_english);
//p
        Log.d("audio", "startAudio: " + player.getDuration());
        Log.d("audio", "startAudio: " + player.getCurrentPosition());


        seekbar.setMax(player.getDuration());
//

        totaltime.setText(String.format("%d:%d",
                TimeUnit.MILLISECONDS.toMinutes(player.getDuration()),
                TimeUnit.MILLISECONDS.toSeconds(player.getDuration()) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(player.getDuration()))));

        if (player != null) {
            player.start();
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (player != null && player.isPlaying()) {
                                ongoingtime.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        ongoingtime.setText(String.format("%d:%d",
                                                TimeUnit.MILLISECONDS.toMinutes(player.getCurrentPosition()),
                                                TimeUnit.MILLISECONDS.toSeconds(player.getCurrentPosition()) -
                                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(player.getCurrentPosition()))));
                                        seekbar.setProgress(player.getCurrentPosition());
                                    }
                                });
                            } else {
                                timer.cancel();
                                timer.purge();
                            }
                        }
                    });
                }
            }, 0, 1000);
        }
    }


    private void stop() {
        try {
            if (player.isPlaying() || player != null) {
                player.stop();
                button_play.setText("Play");
                button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_play_arrow_black_24dp, 0, 0);
                seekbar.setProgress(0);
            } else {
                startAudio(0);
                stop();
            }
        } catch (NullPointerException e) {
            Log.d("ttt", "onClick: " + e);
        }
    }


    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            MemoriesSliders.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewpager.getCurrentItem() < 2) {
                        viewpager.setCurrentItem(viewpager.getCurrentItem() + 1);
                    } else {
                        viewpager.setCurrentItem(0);
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        stop();
        Intent environmentPage = new Intent(MemoriesSliders.this, GroupTourActivity.class);
        environmentPage.putExtra("audio_in", audio_in);
        startActivity(environmentPage);
        finish();
    }
}
