package museum.m_educate.com.museum;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class HeritageActivity extends AppCompatActivity {
    Button units, founder, history, heritage, gaw,cont,back;
    private String audio_in = "Hindi";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.heritage);

        units = (Button) findViewById(R.id.units);
        founder = (Button) findViewById(R.id.founder);
//        history = (Button) findViewById(R.id.history);
        heritage = (Button) findViewById(R.id.heritage);
        gaw = (Button) findViewById(R.id.gwa);
        cont= (Button) findViewById(R.id.cont);
        back = (Button) findViewById(R.id.back_button);
        try {
            audio_in = getIntent().getStringExtra("audio_in");

        } catch (Exception e) {
            Toast.makeText(this, "Audio not selected", Toast.LENGTH_LONG).show();
        }
        units.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                wall8
//                Intent environmentPage = new Intent(HeritageActivity.this, WallExplanation.class);
//                environmentPage.putExtra("audio_english",R.raw.wall_eight_english);
//                environmentPage.putExtra("audio_hindi",R.raw.hindiwall_eight);
//                environmentPage.putExtra("image",R.drawable.no_preview);
//                environmentPage.putExtra("audio_in",audio_in);
//                environmentPage.putExtra("from","heritage");
//                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(environmentPage);
//                finish();
            }
        });

        founder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent environmentPage = new Intent(HeritageActivity.this, WallExplanation.class);
//                environmentPage.putExtra("audio_english",R.raw.wall_nine_english);
//                environmentPage.putExtra("audio_hindi",R.raw.hindiwall_nine);
//                environmentPage.putExtra("image",R.drawable.no_preview);
//                environmentPage.putExtra("audio_in",audio_in);
//                environmentPage.putExtra("from","heritage");
//                environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(environmentPage);
//                finish();
//wall9
            }
        });
//        history.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
        heritage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        gaw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(HeritageActivity.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(HeritageActivity.this, GroupTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });


    }

    @Override
    public void onBackPressed() {
        Intent environmentPage = new Intent(HeritageActivity.this, GroupTourActivity.class);
        environmentPage.putExtra("audio_in", audio_in);
        startActivity(environmentPage);
        finish();
    }
}