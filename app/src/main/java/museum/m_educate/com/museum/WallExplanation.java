package museum.m_educate.com.museum;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import me.relex.circleindicator.CircleIndicator;

public class WallExplanation extends AppCompatActivity {
    Button button_language, button_play, button_pause, button_stop, button_continue, button_back,
    nextwall,backwall;
    SeekBar seekbar;
    ViewPager viewpager;
    TextView totaltime, ongoingtime;
    ImageView imageview;
    final MediaPlayer[] mPlayer2 = new MediaPlayer[1];
    String audio_in = "Hindi";
    Handler seekHandler = new Handler();
    MediaPlayer player;
    private Handler mHandler = new Handler();
    private Timer timer;
    int xaa = 0;
    int audio = 0;
    private int audio_hindi = R.raw.welcome_hindi;
    private int audio_english = R.raw.splash_weclcome_english;
    private String from = "";
    int count;
    boolean SlideChangedone=true;
    boolean SlideChangedtwo = true;
    boolean SlideChangedthree = true;
    boolean SlideChangedfour = true;
    boolean SlideChangedfive = true;
    boolean SlideChangedsix = true;
    boolean SlideChangedseven = true;
    boolean SlideChangedeight = true;
    boolean SlideChangednine = true;
    boolean SlideChangedten=true;
    boolean SlideChangedeleven=true;
    int wall_position;
MyCustomPagerAdapter myCustomPagerAdapter;

    //TODO for Next and Previos button.

    final int[] wall_audio_eng={
            R.raw.wall_eng_one,R.raw.archives,R.raw.wall_eng_two,
            R.raw.wall_eng_three,R.raw.wall_eng_four,R.raw.wall_eng_five,
            R.raw.wall_eng_six,R.raw.wall_eng_seven,R.raw.wall_eng_eight,
            R.raw.wall_eng_nine,R.raw.wall_eng_ten,R.raw.wall_eng_eleven,
            R.raw.wall_eng_twelve,R.raw.wall_eng_thirteen,R.raw.wall_eng_fourteen_chakras,
            R.raw.wall_eng_fiftheen_and_sixteen_veer_shaurya_chakra,R.raw.wall_eng_seventeen,
            R.raw.wall_eng_eighteen,R.raw.wall_eng_nineteen,R.raw.wall_eng_twenty,
            R.raw.wall_eng_twentyone,R.raw.wall_eng_twentytwo,R.raw.wall_eng_twentythree_thirty_honorary_colonel,R.raw.wall_eng_thirtyone,R.raw.wall_eng_thirtytwo,
            R.raw.wall_eng_thirtythree,R.raw.wall_eng_thirtyfour,R.raw.wall_eng_thirtyfive,
            R.raw.wall_eng_thirtysix,R.raw.wall_eng_thirtyseven,R.raw.wall_eng_thirtyeight,
            R.raw.wall_eng_thirtynine,R.raw.wall_eng_fourty,R.raw.wall_eng_fourtyone,
            R.raw.wall_eng_fourtytwo,R.raw.wall_eng_fourtythree,R.raw.wall_eng_fourtyfour,
            R.raw.wall_eng_fourtyfive,R.raw.wall_eng_fourtysix,R.raw.wall_eng_fourtyseven_two_colonel_chief,
            R.raw.wall_eng_fourtyeight_two_honorary_colonels,R.raw.wall_eng_fourtynine_the_colonel,R.raw.wall_eng_fifty_army_commandants,
            R.raw.wall_eng_fiftyone_centre_commandants,
            //wall fifty two not available
            R.raw.wall_eng_fiftyone_centre_commandants,
            R.raw.wall_eng_fiftythree_two_sportstwo,
            R.raw.wall_eng_fiftyfour_two_adventure,R.raw.wall_eng_fiftyfive_rdp,R.raw.wall_eng_fiftysix,
            R.raw.wall_eng_fiftyseven_two_un_missions,R.raw.wall_eng_fiftyeight,R.raw.wall_eng_fiftynine,
            R.raw.wall_eng_sixty,R.raw.wall_eng_sixtyone,R.raw.wall_eng_sixtytwo,
            R.raw.wall_eng_sixtythree,R.raw.wall_eng_sixtyfour_chakras,R.raw.wall_eng_sixtyfive_mangalore_battle,
            R.raw.wall_eng_sixtysix,R.raw.wall_eng_sixtyseven_nineteenseventyone_one,R.raw.wall_eng_sixtyeight_two_nineteenseventyone_two,
            R.raw.wall_eng_sixtynine_two_nineteenseventyone_three,R.raw.wall_eng_seventy_two_nineteenseventyone_four,R.raw.wall_eng_seventyone_nineteenseventyone_five,
            R.raw.wall_eng_seventytwo_two_twentyone_rr,R.raw.wall_eng_seventythree_two_ciops,R.raw.wall_eng_seventyfour,
            R.raw.wall_eng_seventyfive,R.raw.wall_eng_seventysix_seventyseven_missile,R.raw.wall_eng_seventyeight_eightyone_bmp,R.raw.wall_eng_eighttwo_t_fiftyfive_tank

    };
    final int[] wall_audio_hindi={
            R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,R.raw.wall_hindi_one,
            R.raw.wall_hindi_one,
    };

    final int[] slider_default={R.drawable.noimage};
//    final int[] slider_zero={R.drawable.noimage};
//    final int[] slider_one={R.drawable.noimage};
//    final int[] slider_two={R.drawable.wall_two_one,R.drawable.wall_two_two,R.drawable.wall_two_three};
//    final int[] slider_three={R.drawable.wall_three_one,R.drawable.wall_three_two,R.drawable.wall_three_three,R.drawable.wall_three_four};
//    final int[] slider_four={R.drawable.wall_four_one,R.drawable.wall_four_two_two,R.drawable.wall_four_three,R.drawable.wall_four_four,R.drawable.wall_four_five};
    final int[] slider_zero={R.drawable.wall_one_one,R.drawable.wall_one_two,R.drawable.wall_one_three,R.drawable.wall_one_four,R.drawable.wall_one_five,R.drawable.wall_one_six,R.drawable.wall_one_seven,R.drawable.wall_one_eight,R.drawable.wall_one_nine,R.drawable.wall_one_ten,R.drawable.wall_one_eleven};
    final int[] slider_one={R.drawable.noimage};
    final int[] slider_two={R.drawable.two_one,R.drawable.two_two,R.drawable.two_three,R.drawable.two_four,R.drawable.two_five,R.drawable.two_six,R.drawable.two_seven,R.drawable.two_eight,R.drawable.two_nine};
    final int[] slider_three={R.drawable.three_one,R.drawable.three_two,R.drawable.three_three,R.drawable.three_four,R.drawable.three_five,R.drawable.three_six,R.drawable.three_seven,R.drawable.three_eight,R.drawable.three_nine,R.drawable.three_ten,R.drawable.three_eleven};
    final int[] slider_four={R.drawable.four_one,R.drawable.four_two,R.drawable.four_three,R.drawable.four_four,R.drawable.four_five,R.drawable.four_six,R.drawable.four_seven,R.drawable.four_eight,R.drawable.four_nine,R.drawable.four_ten};

    final int[] slider_five={R.drawable.wall_five_one_two,R.drawable.wall_five_two_two,R.drawable.wall_five_three,R.drawable.wall_five_four_two};
    final int[] slider_six={R.drawable.wall_six_one,R.drawable.wall_six_two,R.drawable.wall_six_three,R.drawable.wall_six_four};
    final int[] slider_seven={R.drawable.wall_seven_one_two,R.drawable.wall_seven_two_two,R.drawable.wall_seven_three_two,R.drawable.wall_seven_four,R.drawable.wall_seven_five,R.drawable.wall_seven_eleven};
    final int[] slider_eight={R.drawable.wall_eight_one};
    final int[] slider_nine={R.drawable.wall_nine_one};
    final int[] slider_ten={R.drawable.wall_ten_two,R.drawable.wall_ten_three};

    final int[] slider_eleven={R.drawable.wall_eleven_two,R.drawable.wall_eleven_three};
    final int[] slider_twelve={R.drawable.noimage,R.drawable.noimage,R.drawable.noimage};
    final int[] slider_thirteen={R.drawable.wall_thirteen_two};
    final int[] slider_fourteen={R.drawable.wall_fourteen_one_two,R.drawable.wall_fourteen_two,R.drawable.wall_fourteen_four};
    final int[] slider_fifthteen={R.drawable.wall_fifteen_one,R.drawable.wall_sixteen_one};
    //            final int[] slider_sixteen={R.drawable.wall_sixteen_one};
    final int[] slider_seventeen={R.drawable.wall_seventeen_one};
    final int[] slider_eighteen={R.drawable.wall_eighteen_one};
    final int[] slider_nineteen={R.drawable.wall_ninteen_one};
    final int[] slider_twenty={R.drawable.wall_twenty_point_one,R.drawable.wall_twenty_point_two,R.drawable.wall_twenty_point_three};

    final int[] slider_twentyone={R.drawable.wall_twentyone_one};
    final int[] slider_twentytwo={R.drawable.wall_twentytwo_one};
    final int[] slider_twentythree={R.drawable.wall_twentythree_one,R.drawable.wall_twentyfour_one,
            R.drawable.wall_twentyfive,R.drawable.wall_twentysix_one,R.drawable.wall_twentyseven_one,
            R.drawable.wall_twentyeight_one,R.drawable.wall_twentynine_one,
            R.drawable.wall_thirty_point_one,R.drawable.wall_thirty_point_two,R.drawable.wall_thirty_point_three
    };
    //        final int[] slider_twentyfour={R.drawable.wall_twentyfour_one};
//            final int[] slider_twentyfive={R.drawable.wall_twentyfive};
//            final int[] slider_twentysix={R.drawable.wall_twentysix_one};
//            final int[] slider_twentyseven={R.drawable.wall_twentyseven_one};
//            final int[] slider_twentyeight={R.drawable.wall_twentyeight_one};
//            final int[] slider_twentynine={R.drawable.wall_twentynine_one};
    final int[] slider_thirty={R.drawable.wall_thirty_point_one,R.drawable.wall_thirty_point_two,R.drawable.wall_thirty_point_three};

    final int[] slider_thirtyone={R.drawable.wall_thirtyone_one_two,R.drawable.wall_thirtyone_two,R.drawable.wall_thirtyone_three,R.drawable.wall_thirtyone_four};
    final int[] slider_thirtytwo={R.drawable.wall_thirtytwo_one_two,R.drawable.wall_thirtytwo_two};
    final int[] slider_thirtythree={R.drawable.wall_thirtythree_one_two,R.drawable.wall_thirtythree_two};
    final int[] slider_thirtyfour={R.drawable.wall_thirtyfour_one,R.drawable.wall_thirtyfour_two};
    final int[] slider_thirtyfive={R.drawable.wall_thirtyfive_one,R.drawable.wall_thirtyfive_two};
    final int[] slider_thirtysix={R.drawable.wall_thirtysix_one,R.drawable.wall_thirtysix_two};
    final int[] slider_thirtyseven={R.drawable.wall_thirtyseven_two};
    final int[] slider_thirtyeight={R.drawable.wall_thirtyeight_one,R.drawable.wall_thirtyeight_two};
    final int[] slider_thirtynine={R.drawable.wall_thirtynine_one,R.drawable.wall_thirtynine_two,R.drawable.wall_thirtynine_three,R.drawable.wall_thirtynine_four};
    final int[] slider_fourty={R.drawable.wall_fourty_point_one,R.drawable.wall_fourty_point_two};
    final int[] slider_fourtyone={R.drawable.wall_fourtyone_one,R.drawable.wall_fourtyone_two};
    final int[] slider_fourtytwo={R.drawable.noimage,R.drawable.noimage,R.drawable.noimage};
    final int[] slider_fourtythree={R.drawable.wall_fourtythree_one,R.drawable.wall_fourtythree_two,R.drawable.wall_fourtythree_four,R.drawable.wall_fourtythree_five};
    final int[] slider_fourtyfour={R.drawable.wall_fourtyfour_one};
    final int[] slider_fourtyfive={R.drawable.wall_fourtyfive_one,R.drawable.wall_fourtyfive_two};
    final int[] slider_fourtysix={R.drawable.wall_fourtysix_one,R.drawable.wall_fourtysix_two,R.drawable.wall_fourtysix_four,R.drawable.wall_fourtysix_five,R.drawable.wall_fourtysix_seven};
    final int[] slider_fourtyseven={R.drawable.wall_fourtyseven_one};
    final int[] slider_fourtyeight={R.drawable.wall_fourtyeight_one,R.drawable.wall_fourtyeight_two};
    final int[] slider_fourtynine={R.drawable.wall_fourtynine_one,R.drawable.wall_fourtynine_two,R.drawable.wall_fourtynine_three,R.drawable.wall_fourtynine_four,R.drawable.wall_fourtynine_five};
    final int[] slider_fifty={R.drawable.noimage};
    final int[] slider_fiftyone={R.drawable.noimage};
    final int[] slider_fiftytwo={R.drawable.noimage};
    final int[] slider_fiftythree={R.drawable.noimage};
    final int[] slider_fiftyfour={R.drawable.noimage};
    final int[] slider_fiftyfive={R.drawable.noimage};
    final int[] slider_fiftysix={R.drawable.noimage};
    final int[] slider_fiftyseven={R.drawable.noimage};
    final int[] slider_fiftyeight={R.drawable.noimage};
    final int[] slider_fiftynine={R.drawable.noimage};
    final int[] slider_sixty={R.drawable.noimage};
    final int[] slider_sixtyone={R.drawable.noimage};
    final int[] slider_sixtytwo={R.drawable.noimage};
    final int[] slider_sixtythree={R.drawable.noimage};
    final int[] slider_sixtyfour={R.drawable.noimage};
    final int[] slider_sixtyfive={R.drawable.noimage};
    final int[] slider_sixtysix={R.drawable.noimage};
    final int[] slider_sixtyseven={R.drawable.noimage};
    final int[] slider_sixtyeight={R.drawable.noimage};
    final int[] slider_sixtynine={R.drawable.noimage};
    final int[] slider_seventy={R.drawable.noimage};
    final int[] slider_seventyone={R.drawable.noimage};
    final int[] slider_seventytwo={R.drawable.noimage};
    final int[] slider_seventythree={R.drawable.noimage};
    final int[] slider_seventyfour={R.drawable.noimage};
    final int[] slider_seventyfive={R.drawable.noimage};
    final int[] slider_seventysix={R.drawable.noimage};
    final int[] slider_seventyeight={R.drawable.noimage};
    final int[] slider_eightytwo={R.drawable.noimage};
    final int[] slider_eightythree={R.drawable.noimage};
    final int[] slider_eightyfour={R.drawable.noimage};


    int[][] slider_array ={
            slider_zero,
            slider_one,
            slider_two,
            slider_three,
            slider_four,
            slider_five,
            slider_six,
            slider_seven,
            slider_eight,
            slider_nine,
            slider_ten,
            slider_eleven,
            slider_twelve,
            slider_thirteen,
            slider_fourteen,
            slider_fifthteen,
            slider_seventeen,
            slider_eighteen,
            slider_nine,
            slider_twenty,
            slider_twentyone,
            slider_twentytwo,
            slider_twentythree,
            slider_thirtyone,
            slider_thirtytwo,
            slider_thirtythree,
            slider_thirtyfour,
            slider_thirtyfive,
            slider_thirtysix,
            slider_thirtyseven,
            slider_thirtyeight,
            slider_thirtynine,
            slider_fourty,
            slider_fourtyone,
            slider_fourtytwo,
            slider_fourtythree,
            slider_fourtyfour,
            slider_fourtyfive,
            slider_fourtysix,
            slider_fourtyseven,
            slider_fourtyeight,
            slider_fourtynine,
            slider_fifty,
            slider_fiftyone,
            slider_fiftytwo,
            slider_fiftythree,
            slider_fiftyfour,
            slider_fiftyfive,
            slider_fiftysix,
            slider_fiftyseven,
            slider_fiftyeight,
            slider_fiftynine,
            slider_sixty,
            slider_sixtyone,
            slider_sixtytwo,
            slider_sixtythree,
            slider_sixtyfour,
            slider_sixtyfive,
            slider_sixtysix,
            slider_sixtyseven,
            slider_sixtyeight,
            slider_sixtynine,
            slider_seventy,
            slider_seventyone,
            slider_seventytwo,
            slider_seventythree,
            slider_seventyfour,
            slider_seventyfive,
            slider_seventysix,
            slider_seventyeight,
            slider_eightytwo,
            slider_eightythree,
            slider_eightyfour };

String [] walls_time= {"wall_2",
        "wall_2",
        "wall_3",
        "wall_4",
        "wall_5",
        "wall_6",
        "wall_8",
        "wall_31",
        "wall_32",
        "wall_33",
        "wall_34",
        "wall_35",
        "wall_37",
        "wall_41",
        "wall_46",
        "wall_48",
        "wall_2",
        "wall_4",
        "wall_5",
        "wall_8",
        "wall_7",
        "wall_7",
        "wall_21", "wall_21", "wall_4", "wall_5", "wall_47", "wall_17", "wall_17", "wall_2",
        "wall_21", "wall_21", "wall_4", "wall_5", "wall_47", "wall_17", "wall_17", "wall_2",
        "wall_21", "wall_21", "wall_4", "wall_5", "wall_47", "wall_17", "wall_17", "wall_2",
        "wall_21", "wall_21", "wall_4", "wall_5", "wall_47", "wall_17", "wall_17", "wall_2",
        "wall_21", "wall_21", "wall_4", "wall_5", "wall_47", "wall_17", "wall_17", "wall_2",
        "wall_21", "wall_21", "wall_4", "wall_5", "wall_47", "wall_17", "wall_17", "wall_2",
        "wall_21", "wall_21", "wall_4", "wall_5", "wall_47", "wall_17", "wall_17", "wall_2",
        "wall_21", "wall_21", "wall_4", "wall_5", "wall_47", "wall_17", "wall_17", "wall_2",
        "wall_21", "wall_21", "wall_4", "wall_5", "wall_47", "wall_17", "wall_17", "wall_2",
        "wall_21", "wall_21", "wall_4", "wall_5", "wall_47", "wall_17", "wall_17", "wall_2",


};
    //TODO for Next and Previos button.


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        int result = this.getResources().getConfiguration().orientation;
//        if (result == 1)
//        {
//            Log.d("oncreate", "onCreate: portait");
//            setContentView(R.layout.wall_explanation);
//        }else{
//            Log.d("oncreate", "onCreate: landscape");
            setContentView(R.layout.landscape_wall_explanation);
//        }


        button_continue = (Button) findViewById(R.id.cont);
        button_language = (Button) findViewById(R.id.language);
        button_back = (Button) findViewById(R.id.back_button);
//        button_pause = (Button) findViewById(R.id.pause);
        button_play = (Button) findViewById(R.id.paly);
        button_play.setText("Play");
        button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_play_arrow_black_24dp, 0, 0);
        button_stop = (Button) findViewById(R.id.stop);
        totaltime = (TextView) findViewById(R.id.total_time);
        imageview = (ImageView) findViewById(R.id.imageView2);
        ongoingtime = (TextView) findViewById(R.id.ongoing_time);
        seekbar = (SeekBar) findViewById(R.id.seekbar);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        nextwall = (Button) findViewById(R.id.nextwall);
        backwall = (Button) findViewById(R.id.backwall);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        wall_position = getIntent().getIntExtra("position", 0);
        Log.d("position", "onCreate: " + wall_position);

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean input) {
                if (input){
                    player.stop();
                    SlideChangedone=true;
                    SlideChangedtwo=true;
                    SlideChangedthree=true;
                    SlideChangedfour=true;
                    SlideChangedfive=true;
                    SlideChangedsix=true;
                    SlideChangedseven=true;
                    SlideChangedeight=true;
                    SlideChangednine=true;
                    SlideChangedten=true;
                    SlideChangedeleven=true;
                    Log.d("seek", "onProgressChanged: ");
                    startAudio(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.d("seek", "onStartTrackingTouch: " + seekBar.getProgress());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d("seek", "onStopTrackingTouch : " + seekBar.getProgress());
            }
        });


        //TODO Nextwall onclick
        nextwall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wall_position < 70) {
                    if (("data_adapter").equalsIgnoreCase(from)) {

                        stop();
                        Log.d("position", "position : " + wall_position);
                        Intent environmentPage = new Intent(WallExplanation.this, WallExplanation.class);
                        environmentPage.putExtra("audio_english", wall_audio_eng[wall_position + 1]);
                        environmentPage.putExtra("audio_hindi", wall_audio_hindi[wall_position + 1]);
//                    environmentPage.putExtra("image", walls.get().getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("wall_10",walls_time[wall_position]);
                        environmentPage.putExtra("position", wall_position + 1);
                        environmentPage.putExtra("sliders", slider_array[wall_position + 1]);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(environmentPage);
                        finish();
                    }
                }
                else {
                    Toast.makeText(WallExplanation.this,"This is Last Wall", Toast.LENGTH_LONG).show();
                }
            }
        });
        backwall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wall_position > 0)
                {
                    if (("data_adapter").equalsIgnoreCase(from)) {

                        stop();
                        Log.d("position", "position : " + wall_position);
                        Intent environmentPage = new Intent(WallExplanation.this, WallExplanation.class);
                        environmentPage.putExtra("audio_english", wall_audio_eng[wall_position - 1]);
                        environmentPage.putExtra("audio_hindi", wall_audio_hindi[wall_position - 1]);
//                      environmentPage.putExtra("image", walls.get().getImage_url());
                        environmentPage.putExtra("audio_in", audio_in);
                        environmentPage.putExtra("position", wall_position - 1);
                        environmentPage.putExtra("sliders", slider_array[wall_position - 1]);
                        environmentPage.putExtra("from", "data_adapter");
                        environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(environmentPage);
                        finish();
                    }
                }else {
                    Toast.makeText(WallExplanation.this,"This is First Wall", Toast.LENGTH_LONG).show();
                }
            }
        });


        int[] mResources = {
                R.drawable.noimage,
                R.drawable.noimage_one,
                R.drawable.noimage_two };



if(getIntent().hasExtra("sliders")) {
    int[] slider = getIntent().getExtras().getIntArray("sliders");
    Log.d("sliders", "onCreate: " + slider);
    count = slider.length;
    Log.d("sliders", "onCreate: " + count);
    Log.d("sliders", "onCreate: " + slider);

    myCustomPagerAdapter = new MyCustomPagerAdapter(WallExplanation.this, slider);
    viewpager.setAdapter(myCustomPagerAdapter);
    indicator.setViewPager(viewpager);
}else{

    myCustomPagerAdapter = new MyCustomPagerAdapter(WallExplanation.this, mResources);
    viewpager.setAdapter(myCustomPagerAdapter);
    indicator.setViewPager(viewpager);

}
//        Timer timer = new Timer();
//        timer.scheduleAtFixedRate(new WallExplanation.SliderTimer(), 8000, 10000);










        try {
            audio_in = getIntent().getStringExtra("audio_in");
            Log.d("audio", "onCreate:wall explanation "+audio_in);
            if (audio_in.equalsIgnoreCase("English")) {
                button_language.setText(getApplicationContext().getString(R.string.hindi_language));
            } else if (audio_in.equalsIgnoreCase("Hindi")) {
                button_language.setText("English");
            }
        } catch (Exception e) {
            Toast.makeText(this, "Audio not selected", Toast.LENGTH_LONG).show();
        }


        audio_hindi = getIntent().getIntExtra("audio_hindi", 0);
        audio_english = getIntent().getIntExtra("audio_english", 0);
        from = getIntent().getStringExtra("from");
        Log.d("wall", "onCreate: " + audio_english + "  hindi  " + audio_hindi);
        int image = getIntent().getIntExtra("image", 0);

        if (("earlyguradunits").equalsIgnoreCase(from) || ("awardwinners").equalsIgnoreCase(from)|| ("grouptouractivity").equalsIgnoreCase(from)||
                ("sportadventureactivity").equalsIgnoreCase(from)||("battlehonours").equalsIgnoreCase(from)||("ciops").equalsIgnoreCase(from)
                ||("cutmodels").equalsIgnoreCase(from)
                ||("founder").equalsIgnoreCase(from)
                ||("colonels").equalsIgnoreCase(from)|| ("unit_data_adapter").equalsIgnoreCase(from)) {
            nextwall.setVisibility(View.GONE);
            backwall.setVisibility(View.GONE);
        }

        imageview.setImageResource(image);
        startAudio(xaa);


        button_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stop();
                Intent environmentPage = new Intent(WallExplanation.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });

        button_play.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean b = true;
                if (button_play.getText().equals("Play") && b == true) {
                    startAudio(0);
                    button_play.setText("Pause");
                    button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_pause_black_24dp, 0, 0);

                    b = false;
                } else if (button_play.getText().equals("Pause")) {
                    xaa = player.getCurrentPosition();
                    player.pause();
                    button_play.setText("Resume");
                    button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_play_arrow_black_24dp, 0, 0);

                    b = false;
                } else if (button_play.getText().equals("Resume") && b == true) {
                    player.seekTo(xaa);
                    startAudio(xaa);
                    button_play.setText("Pause");
                    button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_pause_black_24dp, 0, 0);

                    b = false;
                }


            }

        });


        button_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button_play.setText("Play");
                button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_play_arrow_black_24dp, 0, 0);
                seekbar.setProgress(0);
                stop();
            }
        });

        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (("heritage").equalsIgnoreCase(from)) {
                    stop();
                    Intent environmentPage = new Intent(WallExplanation.this, HeritageActivity.class);
                    environmentPage.putExtra("audio_in", audio_in);
                    startActivity(environmentPage);
                    finish();
                }else if (("unit_data_adapter").equalsIgnoreCase(from)) {
                    stop();
                    Intent environmentPage = new Intent(WallExplanation.this, UnitsActivity.class);
                    environmentPage.putExtra("audio_in", audio_in);
                    startActivity(environmentPage);
                    finish();
                }
                else if (("earlyguradunits").equalsIgnoreCase(from)) {
                    stop();
                    Intent environmentPage = new Intent(WallExplanation.this, EarlyGuardUnits.class);
                    environmentPage.putExtra("audio_in", audio_in);
                    startActivity(environmentPage);
                    finish();

                }
                else if (("awardwinners").equalsIgnoreCase(from)) {
                    stop();
                    Intent environmentPage = new Intent(WallExplanation.this, AwardWinners.class);
                    environmentPage.putExtra("audio_in", audio_in);
                    startActivity(environmentPage);
                    finish();
                }
                else if (("grouptouractivity").equalsIgnoreCase(from)) {
                    stop();
                    Intent environmentPage = new Intent(WallExplanation.this, GroupTourActivity.class);
                    environmentPage.putExtra("audio_in", audio_in);
                    startActivity(environmentPage);
                    finish();
                }
                else if (("sportadventureactivity").equalsIgnoreCase(from)) {
                    stop();
                    Intent environmentPage = new Intent(WallExplanation.this, SportAdventureActivity.class);
                    environmentPage.putExtra("audio_in", audio_in);
                    startActivity(environmentPage);
                    finish();
                }
                else if (("battlehonours").equalsIgnoreCase(from)) {
                    stop();
                    Intent environmentPage = new Intent(WallExplanation.this, BattleHonours.class);
                    environmentPage.putExtra("audio_in", audio_in);
                    startActivity(environmentPage);
                    finish();
                }
                else if (("ciops").equalsIgnoreCase(from)) {
                    stop();
                    Intent environmentPage = new Intent(WallExplanation.this, CiOps.class);
                    environmentPage.putExtra("audio_in", audio_in);
                    startActivity(environmentPage);
                    finish();
                }
                else if (("cutmodels").equalsIgnoreCase(from)) {
                    stop();
                    Intent environmentPage = new Intent(WallExplanation.this, CutModels.class);
                    environmentPage.putExtra("audio_in", audio_in);
                    startActivity(environmentPage);
                    finish();
                }
                else if (("founder").equalsIgnoreCase(from)) {
                    stop();
                    Intent environmentPage = new Intent(WallExplanation.this, GroupTourActivity.class);
                    environmentPage.putExtra("audio_in", audio_in);
                    startActivity(environmentPage);
                    finish();
                }
                else if (("colonels").equalsIgnoreCase(from)) {
                    stop();
                    Intent environmentPage = new Intent(WallExplanation.this, ColonelsCentreCommandants.class);
                    environmentPage.putExtra("audio_in", audio_in);
                    startActivity(environmentPage);
                    finish();
                }
                else{
                    stop();
                    Intent environmentPage = new Intent(WallExplanation.this, SequencialTourActivity.class);
                    environmentPage.putExtra("audio_in", audio_in);
                    startActivity(environmentPage);
                    finish();
                }
            }
        });

        button_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("audio", "onClick: " + button_language.getText());
                if (button_language.getText().equals("Language")) {
                    showLanguge();
                } else if (button_language.getText().equals("English")) {
                    stop();
                    audio_in = "English";
                    startAudio(xaa);
                    button_language.setText(getApplicationContext().getString(R.string.hindi_language));

                } else if (button_language.getText().equals("हिंदी")) {
                    stop();
                    audio_in = "Hindi";
                    startAudio(xaa);
                    button_language.setText("English");
                }
            }
        });


    }

//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//
//        // Checks the orientation of the screen
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            setContentView(R.layout.landscape_wall_explanation);
//            startAudio(xaa);
//        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
//            setContentView(R.layout.wall_explanation);
//            startAudio(xaa);
//        }
//    }

    private void showLanguge() {
        final CharSequence[] photo = {"English", "Hindi"};

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Select Language");

        alert.setSingleChoiceItems(photo, -1, new

                DialogInterface.OnClickListener()

                {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (photo[which] == "English")

                        {
                            audio_in = "English";
                            button_language.setText(getApplicationContext().getString(R.string.hindi_language));
                            xaa = 0;
                            stop();
                            startAudio(xaa);
                            dialog.dismiss();

                        } else if (photo[which] == "Hindi")

                        {

                            audio_in = "Hindi";
                            button_language.setText("Listen in English");
                            xaa = 0;
                            stop();
                            startAudio(xaa);
                            dialog.dismiss();

                        }
                    }

                });
        alert.show();
    }

    private void startAudio(int i) {
        if (audio_in.equalsIgnoreCase("Hindi")) {
            player = MediaPlayer.create(this, audio_hindi);
            player.seekTo(i);
        } else {
            player = MediaPlayer.create(this, audio_english);
            player.seekTo(i);
        }
        button_play.setText("Pause");
        button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_pause_black_24dp, 0, 0);

        player.start();
//        player = MediaPlayer.create(this, R.raw.splash_weclcome_english);
//p
        Log.d("audio", "startAudio: " + player.getDuration());
        Log.d("audio", "startAudio: " + player.getCurrentPosition());


        seekbar.setMax(player.getDuration());
//

        totaltime.setText(String.format("%d:%d",
                TimeUnit.MILLISECONDS.toMinutes(player.getDuration()),
                TimeUnit.MILLISECONDS.toSeconds(player.getDuration()) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(player.getDuration()))));

        if (player != null) {
            player.start();
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (player != null && player.isPlaying()) {
                                ongoingtime.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        ongoingtime.setText(String.format("%d:%d",
                                                TimeUnit.MILLISECONDS.toMinutes(player.getCurrentPosition()),
                                                TimeUnit.MILLISECONDS.toSeconds(player.getCurrentPosition()) -
                                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(player.getCurrentPosition()))));

                                        Log.d("seek", "ongoing " + ongoingtime.getText());
                                        Log.d("seek", "ongoing @@@@" + Long.valueOf (TimeUnit.MILLISECONDS.toMillis(player.getCurrentPosition())));
                                        long i = Long.valueOf (TimeUnit.MILLISECONDS.toMillis(player.getCurrentPosition()));


                                        //todo wall Position 0
                                        if(wall_position == 0) {
                                            if(SlideChangedone) {
                                                if (i > 34500) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 49100) {
                                                    changeSlide(2);
                                                    SlideChangedtwo = false;

                                                }
                                            }

                                            if(SlideChangedthree) {
                                                if (i > 59500) {
                                                    changeSlide(3);
                                                    SlideChangedthree = false;

                                                }
                                            }

                                            if(SlideChangedfour) {
                                                if (i > 76100) {
                                                    changeSlide(4);
                                                    SlideChangedfour = false;

                                                }
                                            }
                                            if(SlideChangedfive) {
                                                if (i > 90300) {
                                                    changeSlide(5);
                                                    SlideChangedfive = false;

                                                }
                                            }
                                            if(SlideChangedsix) {
                                                if (i > 114000) {
                                                    changeSlide(6);
                                                    SlideChangedsix = false;

                                                }
                                            }
                                            if(SlideChangedseven) {
                                                if (i > 149000) {
                                                    changeSlide(7);
                                                    SlideChangedseven = false;

                                                }
                                            }
                                            if(SlideChangedeight) {
                                                if (i > 157000) {
                                                    changeSlide(8);
                                                    SlideChangedeight = false;

                                                }
                                            }
                                            if(SlideChangednine) {
                                                if (i > 175000) {
                                                    changeSlide(9);
                                                    SlideChangednine = false;

                                                }
                                            }
                                            if(SlideChangedten) {
                                                if (i > 179000) {
                                                    changeSlide(10);
                                                    SlideChangedten = false;

                                                }
                                            }
                                            if(SlideChangedeleven) {
                                                if (i > 180000) {
                                                    changeSlide(11);
                                                    SlideChangedeleven = false;

                                                }
                                            }
                                        }

                                        //todo wall Position 1
                                        if(wall_position == 1) {
                                            if(SlideChangedone) {
                                                if (i > 64000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 213000) {
                                                    changeSlide(2);
                                                    SlideChangedtwo = false;

                                                }
                                            }
                                        }
                                 if(wall_position ==2) {
                                     if(SlideChangedone) {
                                         if (i > 12400) {
                                             SlideChangedone = false;
                                             changeSlide(1);
                                         }
                                     }
                                     if(SlideChangedtwo) {
                                         if (i > 36400) {
                                             changeSlide(2);
                                             SlideChangedtwo = false;

                                         }
                                     }

                                     if(SlideChangedthree) {
                                         if (i > 94600) {
                                             changeSlide(3);
                                             SlideChangedthree = false;

                                         }
                                     }

                                     if(SlideChangedfour) {
                                         if (i > 274000) {
                                             changeSlide(4);
                                             SlideChangedfour = false;

                                         }
                                     }
                                     if(SlideChangedfive) {
                                         if (i > 277000) {
                                             changeSlide(5);
                                             SlideChangedfive = false;

                                         }
                                     }
                                     if(SlideChangedsix) {
                                         if (i > 283000) {
                                             changeSlide(6);
                                             SlideChangedsix = false;

                                         }
                                     }
                                     if(SlideChangedseven) {
                                         if (i > 298000) {
                                             changeSlide(7);
                                             SlideChangedseven = false;

                                         }
                                     }
                                     if(SlideChangedeight) {
                                         if (i > 312000) {
                                             changeSlide(8);
                                             SlideChangedeight = false;

                                         }
                                     }
                                 }
                                        //todo wall 3
                                        if(wall_position == 3) {
                                            if(SlideChangedone) {
                                                if (i > 182000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 195000) {
                                                    changeSlide(2);
                                                    SlideChangedtwo = false;

                                                }
                                            }
                                            if(SlideChangedthree) {
                                                if (i > 209000) {
                                                    changeSlide(3);
                                                    SlideChangedthree = false;

                                                }
                                            }
                                            if(SlideChangedfour) {
                                                if (i > 222000) {
                                                    changeSlide(4);
                                                    SlideChangedfour = false;

                                                }
                                            }

                                            if(SlideChangedfive) {
                                                if (i > 234000) {
                                                    changeSlide(5);
                                                    SlideChangedfive = false;

                                                }
                                            }
                                            if(SlideChangedsix) {
                                                if (i > 245000) {
                                                    changeSlide(6);
                                                    SlideChangedsix = false;

                                                }
                                            }
                                            if(SlideChangedseven) {
                                                if (i > 256000) {
                                                    changeSlide(7);
                                                    SlideChangedseven = false;

                                                }
                                            }
                                            if(SlideChangedeight) {
                                                if (i > 374000) {
                                                    changeSlide(8);
                                                    SlideChangedeight = false;

                                                }
                                            }
                                            if(SlideChangednine) {
                                                if (i > 385000) {
                                                    changeSlide(9);
                                                    SlideChangednine = false;

                                                }
                                            }
                                            if(SlideChangedten) {
                                                if (i > 392000) {
                                                    changeSlide(10);
                                                    SlideChangedten = false;

                                                }
                                            }
                                            if(SlideChangedeleven) {
                                                if (i > 393000) {
                                                    changeSlide(11);
                                                    SlideChangedeleven = false;

                                                }
                                            }

                                        }
                                        //todo wall 4
                                        if(wall_position == 4) {
                                            if(SlideChangedone) {
                                                if (i > 202000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 227000) {
                                                    changeSlide(2);
                                                    SlideChangedtwo = false;

                                                }
                                            }
                                            if(SlideChangedthree) {
                                                if (i > 234000) {
                                                    changeSlide(3);
                                                    SlideChangedthree = false;

                                                }
                                            }
                                            if(SlideChangedfour) {
                                                if (i > 245000) {
                                                    changeSlide(4);
                                                    SlideChangedfour = false;

                                                }
                                            }

                                            if(SlideChangedfive) {
                                                if (i > 252000) {
                                                    changeSlide(5);
                                                    SlideChangedfive = false;

                                                }
                                            }
                                            if(SlideChangedsix) {
                                                if (i > 267000) {
                                                    changeSlide(6);
                                                    SlideChangedsix = false;

                                                }
                                            }
                                            if(SlideChangedseven) {
                                                if (i > 278000) {
                                                    changeSlide(7);
                                                    SlideChangedseven = false;

                                                }
                                            }
                                            if(SlideChangedeight) {
                                                if (i > 284000) {
                                                    changeSlide(8);
                                                    SlideChangedeight = false;

                                                }
                                            }
                                            if(SlideChangednine) {
                                                if (i > 294000) {
                                                    changeSlide(9);
                                                    SlideChangednine = false;

                                                }
                                            }
                                            if(SlideChangedten) {
                                                if (i > 295000) {
                                                    changeSlide(10);
                                                    SlideChangedten = false;

                                                }
                                            }
                                        }
                                       //TODO for wall 5

                                        if(wall_position==5) {
                                            if(SlideChangedone) {
                                                if (i > 155000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 178000) {
                                                    changeSlide(2);
                                                    SlideChangedtwo = false;

                                                }
                                            }

                                            if(SlideChangedthree) {
                                                if (i > 197000) {
                                                    changeSlide(3);
                                                    SlideChangedthree = false;

                                                }
                                            }
                                            if(SlideChangedfour) {
                                                if (i > 264400) {
                                                    changeSlide(4);
                                                    SlideChangedfour = false;

                                                }
                                            }
                                        }



                                        //Todo for wall 6 slider break not accurate
                                        if(wall_position == 6) {
                                            if(SlideChangedone) {
                                                if (i > 156000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 189000) {
                                                    changeSlide(2);
                                                    SlideChangedtwo = false;
                                                }
                                            }
                                            if(SlideChangedthree) {
                                                if (i > 234000) {
                                                    changeSlide(3);
                                                    SlideChangedthree = false;

                                                }
                                            }
                                        }



                                            //Todo for wall 7
                                        if(wall_position == 7) {
                                            if(SlideChangedone) {
                                                if (i > 128000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 166000) {
                                                    changeSlide(2);
                                                    SlideChangedtwo = false;

                                                }
                                            }
                                            if(SlideChangedthree) {
                                                if (i > 192000) {
                                                    changeSlide(3);
                                                    SlideChangedthree = false;

                                                }
                                            }
                                            if(SlideChangedfour) {
                                                if (i > 255000) {
                                                    changeSlide(4);
                                                    SlideChangedfour = false;

                                                }
                                            }
                                            if(SlideChangedfive) {
                                                if (i > 287000) {
                                                    changeSlide(5);
                                                    SlideChangedfive = false;

                                                }
                                            }
                                        }

                        //todo wall 8 and 9 slider not available

                                        //Todo for wall 10
                                        if(wall_position == 10) {
                                            if(SlideChangedone) {
                                                if (i > 132000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 151000) {
                                                    changeSlide(2);
                                                    SlideChangedtwo = false;

                                                }
                                            }
                                        }

                                        //Todo for wall 11 not proper time
                                        if(wall_position==11) {
                                            if(SlideChangedone) {
                                                if (i > 240000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            }

                                        //Todo for wall 12 not sliders... proper time
                                        if(wall_position==12) {
                                            if(SlideChangedone) {
                                                if (i > 240000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                        }

                                        //todo Wall 13 only one image


                                        //Todo for wall 14
                                        if(wall_position==14) {
                                            if(SlideChangedone) {
                                                if (i > 20000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 25000) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(1);
                                                }
                                            }
                                        }


                                        //Todo for wall 15 .. proper time
                                        if(wall_position==15) {
                                            if(SlideChangedone) {
                                                if (i > 10629) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }

                                        }

                                        //Todo for wall 17 only one image no proper time
//                                        if(wall_position==16) {
//                                            if(SlideChangedone) {
//                                                if (i > 10629) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                        }
                                        //Todo for wall 18 only one image no proper time
//                                        if(wall_position==17) {
//                                            if(SlideChangedone) {
//                                                if (i > 10629) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                        }

                                        //Todo for wall 19 only one image no proper time
//                                        if(wall_position==18) {
//                                            if(SlideChangedone) {
//                                                if (i > 10629) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//
//                                        }

                                        //Todo for wall 20 only one image no proper time
                                        if(wall_position==19) {
                                            if(SlideChangedone) {
                                                if (i > 11000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }

                                            if(SlideChangedtwo) {
                                                if (i > 17000) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(2);
                                                }
                                            }

                                        }


                                        //Todo for wall 21 only one image no proper time
//                                        if(wall_position==20) {
//                                            if (SlideChangedone) {
//                                                if (i > 11000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                        }
                                        //Todo for wall 22 only one image
//                                        if(wall_position==21) {
//                                            if (SlideChangedone) {
//                                                if (i > 11000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                        }


                                        //Todo for wall 23
                                        if(wall_position==22) {
                                            if(SlideChangedone) {
                                                if (i > 21600) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 27000) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(2);
                                                }
                                            }
                                            if(SlideChangedthree) {
                                                if (i > 30700) {
                                                    SlideChangedthree = false;
                                                    changeSlide(3);
                                                }
                                            }
                                            if(SlideChangedfour) {
                                                if (i > 32600) {
                                                    SlideChangedfour = false;
                                                    changeSlide(4);
                                                }
                                            }
                                            if(SlideChangedfive) {
                                                if (i > 35000) {
                                                    SlideChangedfive = false;
                                                    changeSlide(5);
                                                }
                                            }
                                            if(SlideChangedsix) {
                                                if (i > 39200) {
                                                    SlideChangedsix = false;
                                                    changeSlide(6);
                                                }
                                            }
                                            if(SlideChangedseven) {
                                                if (i > 41900) {
                                                    SlideChangedseven = false;
                                                    changeSlide(7);
                                                }
                                            }
                                            if(SlideChangedeight) {
                                                if (i > 48800) {
                                                    SlideChangedeight = false;
                                                    changeSlide(8);
                                                }
                                            }
                                            if(SlideChangednine) {
                                                if (i > 72300) {
                                                    SlideChangednine = false;
                                                    changeSlide(9);
                                                }
                                            }
                                        }






                                        //Todo for wall 31
                                        if(wall_position==23) {
                                            if(SlideChangedone) {
                                                if (i > 147000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 280000) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(2);
                                                }
                                            }
                                            if(SlideChangedthree) {
                                                if (i > 295000) {
                                                    SlideChangedthree = false;
                                                    changeSlide(3);
                                                }
                                            }
                                            if(SlideChangedfour) {
                                                if (i > 333000) {
                                                    SlideChangedfour = false;
                                                    changeSlide(4);
                                                }
                                            }
                                        }


                                        //Todo for wall 32
                                        if(wall_position==24) {
                                            if(SlideChangedone) {
                                                if (i > 111000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 120000) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(2);
                                                }
                                            }
                                        }

                                        //Todo for wall 33
                                        if(wall_position==25) {
                                            if(SlideChangedone) {
                                                if (i > 154000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 177000) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(2);
                                                }
                                            }
                                            if(SlideChangedthree) {
                                                if (i > 186000) {
                                                    SlideChangedthree = false;
                                                    changeSlide(3);
                                                }
                                            }
                                            if(SlideChangedfour) {
                                                if (i > 211000) {
                                                    SlideChangedfour = false;
                                                    changeSlide(4);
                                                }
                                            }
                                        }


                                        //Todo for wall 34
                                        if(wall_position==26) {
                                            if (SlideChangedone) {
                                                if (i > 109000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if (SlideChangedtwo) {
                                                if (i > 115000) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(2);
                                                }
                                            }
                                            if (SlideChangedthree) {
                                                if (i > 115000) {
                                                    SlideChangedthree = false;
                                                    changeSlide(3);
                                                }
                                            }
                                        }


                                        //Todo for wall 35
                                        if(wall_position==27) {
                                            if (SlideChangedone) {
                                                if (i > 112000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if (SlideChangedtwo) {
                                                if (i > 115000) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(0);
                                                }
                                            }

                                            if (SlideChangedthree) {
                                                if (i > 130000) {
                                                    SlideChangedthree = false;
                                                    changeSlide(1);
                                                }
                                            }

                                            if (SlideChangedfour) {
                                                if (i > 165000) {
                                                    SlideChangedfour = false;
                                                    changeSlide(0);
                                                }
                                            }
                                            if (SlideChangedfive) {
                                                if (i > 182000) {
                                                    SlideChangedfive = false;
                                                    changeSlide(1);
                                                }
                                            }
                                        }


                                        //Todo for wall 36
                                        if(wall_position==28) {
                                            if(SlideChangedone) {
                                                if (i > 70000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }

                                            }
                                        }

                                        //Todo for wall 37 only one slider image
//                                        if(wall_position==29) {
//                                            if(SlideChangedone) {
//                                                if (i > 70000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//
//                                            }
//                                        }

                                        //Todo for wall 38
                                        if(wall_position==30) {
                                            if(SlideChangedone) {
                                                if (i > 84000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }

                                            if(SlideChangedtwo) {
                                                if (i > 108000) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(2);
                                                }
                                            }
                                            if(SlideChangedthree) {
                                                if (i >  12900) {
                                                    SlideChangedthree = false;
                                                    changeSlide(1);
                                                }
                                            }
                                        }

                                        //Todo for wall 39
                                        if(wall_position==31) {
                                            if(SlideChangedone) {
                                                if (i > 74000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }

                                            if(SlideChangedtwo) {
                                                if (i > 110000) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(3);
                                                }
                                            }
                                            if(SlideChangedthree) {
                                                if (i >  125000) {
                                                    SlideChangedthree = false;
                                                    changeSlide(4);
                                                }
                                            }
                                            if(SlideChangedfour) {
                                                if (i >  144000) {
                                                    SlideChangedfour = false;
                                                    changeSlide(2);
                                                }
                                            }

                                        }

                                        //Todo for wall 40
                                        if(wall_position==32) {
                                            if(SlideChangedone) {
                                                if (i > 110000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }

                                            if(SlideChangedtwo) {
                                                if (i > 125000) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(2);
                                                }
                                            }
                                            if(SlideChangedthree) {
                                                if (i >  134000) {
                                                    SlideChangedthree = false;
                                                    changeSlide(1);
                                                }
                                             }
                                            }

                                        //Todo for wall 41
                                        if(wall_position==33) {
                                            if(SlideChangedone) {
                                                if (i > 107000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                        }

                                        //todo  wall 42 no slider

//                                        if(wall_position==34) {
//                                            if(SlideChangedone) {
//                                                if (i > 107000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                        }

                                        //Todo for wall 43
                                        if(wall_position==35) {
                                            if(SlideChangedone) {
                                                if (i > 105000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }

                                            if(SlideChangedtwo) {
                                                if (i > 106000) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(2);
                                                }
                                            }
                                            if(SlideChangedthree) {
                                                if (i >  107000) {
                                                    SlideChangedthree = false;
                                                    changeSlide(3);
                                                }
                                            }
//                                            if(SlideChangedfour) {
//                                                if (i >  107000) {
//                                                    SlideChangedfour = false;
//                                                    changeSlide(4);
//                                                }
//                                            }

                                        }


                                        //todo wall 44 not done



                                        //Todo for wall 45
                                        if(wall_position == 37) {
                                            if(SlideChangedone) {
                                                if (i > 82000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                        }

                                        //Todo for wall 46
                                        if(wall_position == 38) {
                                            if(SlideChangedone) {
                                                if (i > 83000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 85000) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(2);
                                                }
                                            }
                                            if(SlideChangedthree) {
                                                if (i > 87000) {
                                                    SlideChangedthree = false;
                                                    changeSlide(3);
                                                }
                                            }
                                            if(SlideChangedfour) {
                                                if (i > 90000) {
                                                    SlideChangedfour = false;
                                                    changeSlide(4);
                                                }
                                            }
                                        }

                                        //Todo for wall 47 only one image
//                                        if(wall_position == 39) {
//                                            if (SlideChangedone) {
//                                                if (i > 83000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                        }

                                        //Todo for wall 48 only one image
                                        if(wall_position == 40) {
                                            if (SlideChangedone) {
                                                if (i > 26000) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                        }

                                        //Todo for wall 49 only one image
                                        if(wall_position == 41) {
                                            if(SlideChangedone) {
                                                if (i > 2700) {
                                                    SlideChangedone = false;
                                                    changeSlide(1);
                                                }
                                            }
                                            if(SlideChangedtwo) {
                                                if (i > 4500) {
                                                    SlideChangedtwo = false;
                                                    changeSlide(2);
                                                }
                                            }
                                            if(SlideChangedthree) {
                                                if (i > 6700) {
                                                    SlideChangedthree = false;
                                                    changeSlide(3);
                                                }
                                            }
                                            if(SlideChangedfour) {
                                                if (i > 8100) {
                                                    SlideChangedfour = false;
                                                    changeSlide(4);
                                                }
                                            }
                                        }





























//                                        //Todo for wall 20
//                                        if(wall_position==31) {
//                                            if(SlideChangedone) {
//                                                if (i > 147000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                            if(SlideChangedtwo) {
//                                                if (i > 280000) {
//                                                    SlideChangedtwo = false;
//                                                    changeSlide(2);
//                                                }
//                                            }
//                                            if(SlideChangedthree) {
//                                                if (i > 295000) {
//                                                    SlideChangedthree = false;
//                                                    changeSlide(3);
//                                                }
//                                            }
//                                            if(SlideChangedfour) {
//                                                if (i > 333000) {
//                                                    SlideChangedfour = false;
//                                                    changeSlide(4);
//                                                }
//                                            }
//                                        }
//
//                                        //Todo for wall 20
//                                        if(wall_position==31) {
//                                            if(SlideChangedone) {
//                                                if (i > 111000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                            if(SlideChangedtwo) {
//                                                if (i > 120000) {
//                                                    SlideChangedtwo = false;
//                                                    changeSlide(2);
//                                                }
//                                            }
//                                            if(SlideChangedthree) {
//                                                if (i > 175000) {
//                                                    SlideChangedthree = false;
//                                                    changeSlide(3);
//                                                }
//                                            }
//                                            if(SlideChangedfour) {
//                                                if (i > 210000) {
//                                                    SlideChangedfour = false;
//                                                    changeSlide(4);
//                                                }
//                                            }
//                                        }
////Todo for wall 20
//
//
//
//                                        //Todo for wall 20
//                                        if(wall_position==31) {
//                                            if(SlideChangedone) {
//                                                if (i > 109000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                            if(SlideChangedtwo) {
//                                                if (i > 115000) {
//                                                    SlideChangedtwo = false;
//                                                    changeSlide(2);
//                                                }
//                                            }
//                                            if(SlideChangedthree) {
//                                                if (i > 128000) {
//                                                    SlideChangedthree = false;
//                                                    changeSlide(3);
//                                                }
//                                            }
//                                            if(SlideChangedfour) {
//                                                if (i > 170000) {
//                                                    SlideChangedfour = false;
//                                                    changeSlide(4);
//                                                }
//                                            }
//                                        }
//
//                                        //Todo for wall 20
//                                        if(getIntent().hasExtra("wall_35")) {
//                                            if(SlideChangedone) {
//                                                if (i > 112000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                            if(SlideChangedtwo) {
//                                                if (i > 117000) {
//                                                    SlideChangedtwo = false;
//                                                    changeSlide(2);
//                                                }
//                                            }
//                                            if(SlideChangedthree) {
//                                                if (i > 165000) {
//                                                    SlideChangedthree = false;
//                                                    changeSlide(3);
//                                                }
//                                            }
//                                            if(SlideChangedfour) {
//                                                if (i > 182000) {
//                                                    SlideChangedfour = false;
//                                                    changeSlide(4);
//                                                }
//                                            }
//                                        }
////Todo for wall 20
//                                        if(getIntent().hasExtra("wall_36")) {
//                                            if(SlideChangedone) {
//                                                if (i > 70000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//
//                                            }
//                                        }
//
//                                        //Todo for wall 20
//                                        if(getIntent().hasExtra("wall_38")) {
//                                            if(SlideChangedone) {
//                                                if (i > 84000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                            if(SlideChangedtwo) {
//                                                if (i > 108000) {
//                                                    SlideChangedtwo = false;
//                                                    changeSlide(2);
//                                                }
//                                            }
//                                            if(SlideChangedthree) {
//                                                if (i > 129000) {
//                                                    SlideChangedthree = false;
//                                                    changeSlide(3);
//                                                }
//
//                                            }
//                                        }
//                                        //Todo for wall 20
//                                        if(wall_position==39) {
//                                            if(SlideChangedone) {
//                                                if (i > 74000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                            if(SlideChangedtwo) {
//                                                if (i > 110000) {
//                                                    SlideChangedtwo = false;
//                                                    changeSlide(2);
//                                                }
//                                            }
//                                            if(SlideChangedthree) {
//                                                if (i > 125000) {
//                                                    SlideChangedthree = false;
//                                                    changeSlide(3);
//                                                }
//                                            }
//                                            if(SlideChangedfour) {
//                                                if (i > 144000) {
//                                                    SlideChangedfour = false;
//                                                    changeSlide(4);
//                                                }
//                                            }
//                                        }
//                                        //Todo for wall 20
//                                        if(getIntent().hasExtra("wall_40")) {
//                                            if(SlideChangedone) {
//                                                if (i > 110000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                            if(SlideChangedtwo) {
//                                                if (i > 125000) {
//                                                    SlideChangedtwo = false;
//                                                    changeSlide(2);
//                                                }
//                                            }
//                                        }
//
//                                        //Todo for wall 20
//                                        if(getIntent().hasExtra("wall_41")) {
//                                            if(SlideChangedone) {
//                                                if (i > 107000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//
//                                            }
//                                        }
////Todo for wall 20
//                                        if(wall_position==43) {
//                                            if(SlideChangedone) {
//                                                if (i > 24000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                            if(SlideChangedtwo) {
//                                                if (i > 52000) {
//                                                    SlideChangedtwo = false;
//                                                    changeSlide(2);
//                                                }
//                                            }
//                                            if(SlideChangedthree) {
//                                                if (i > 80000) {
//                                                    SlideChangedthree = false;
//                                                    changeSlide(3);
//                                                }
//                                            }
//                                        }
//                                        //Todo for wall 20
//                                        if(getIntent().hasExtra("wall_45")) {
//                                            if(SlideChangedone) {
//                                                if (i > 82000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                        }
//                                        //Todo for wall 20
//                                        if(getIntent().hasExtra("wall_46")) {
//                                            if(SlideChangedone) {
//                                                if (i > 83000) {
//                                                    SlideChangedone = false;
//                                                    changeSlide(1);
//                                                }
//                                            }
//                                        }




                                        seekbar.setProgress(player.getCurrentPosition());
                                    }
                                });
                            } else {
                                timer.cancel();
                                timer.purge();
                            }
                        }
                    });
                }
            }, 0, 1000);
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if (wall_position > 0)
                    {
                        if (("data_adapter").equalsIgnoreCase(from)) {

                            stop();
                            Log.d("position", "position : " + wall_position);
                            Intent environmentPage = new Intent(WallExplanation.this, WallExplanation.class);
                            environmentPage.putExtra("audio_english", wall_audio_eng[wall_position - 1]);
                            environmentPage.putExtra("audio_hindi", wall_audio_hindi[wall_position - 1]);
//                      environmentPage.putExtra("image", walls.get().getImage_url());
                            environmentPage.putExtra("audio_in", audio_in);
                            environmentPage.putExtra("position", wall_position - 1);
                            environmentPage.putExtra("sliders", slider_array[wall_position - 1]);
                            environmentPage.putExtra("from", "data_adapter");
                            environmentPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(environmentPage);
                            finish();
                        }
                    }else {
                        Toast.makeText(WallExplanation.this,"This is First Wall", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    public void changeSlide(int index){
        Log.d("seek", "changeSlide: " + count);
        if (viewpager.getCurrentItem() < count)
        {
            viewpager.setCurrentItem(index);

        } else {
            viewpager.setCurrentItem(0);
        }
    }

    private void stop() {
        try {
            if (player.isPlaying() || player != null) {
                player.stop();
                button_play.setText("Play");
                button_play.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_play_arrow_black_24dp, 0, 0);
                seekbar.setProgress(0);
            } else {
                startAudio(0);
                stop();
            }
        } catch (NullPointerException e) {
            Log.d("ttt", "onClick: " + e);
        }
    }



    @Override
    public void onBackPressed() {
        if (("heritage").equalsIgnoreCase(from)) {
            stop();
            Intent environmentPage = new Intent(WallExplanation.this, HeritageActivity.class);
            environmentPage.putExtra("audio_in", audio_in);
            startActivity(environmentPage);
            finish();
        }else if (("unit_data_adapter").equalsIgnoreCase(from)) {
            stop();
            Intent environmentPage = new Intent(WallExplanation.this, UnitsActivity.class);
            environmentPage.putExtra("audio_in", audio_in);
            startActivity(environmentPage);
            finish();
        }
        else if (("earlyguradunits").equalsIgnoreCase(from)) {
            stop();
            Intent environmentPage = new Intent(WallExplanation.this, EarlyGuardUnits.class);
            environmentPage.putExtra("audio_in", audio_in);
            startActivity(environmentPage);
            finish();

        }
        else if (("awardwinners").equalsIgnoreCase(from)) {
            stop();
            Intent environmentPage = new Intent(WallExplanation.this, AwardWinners.class);
            environmentPage.putExtra("audio_in", audio_in);
            startActivity(environmentPage);
            finish();
        }
        else if (("grouptouractivity").equalsIgnoreCase(from)) {
            stop();
            Intent environmentPage = new Intent(WallExplanation.this, GroupTourActivity.class);
            environmentPage.putExtra("audio_in", audio_in);
            startActivity(environmentPage);
            finish();
        }
        else if (("sportadventureactivity").equalsIgnoreCase(from)) {
            stop();
            Intent environmentPage = new Intent(WallExplanation.this, SportAdventureActivity.class);
            environmentPage.putExtra("audio_in", audio_in);
            startActivity(environmentPage);
            finish();
        }
        else if (("battlehonours").equalsIgnoreCase(from)) {
            stop();
            Intent environmentPage = new Intent(WallExplanation.this, BattleHonours.class);
            environmentPage.putExtra("audio_in", audio_in);
            startActivity(environmentPage);
            finish();
        }
        else if (("ciops").equalsIgnoreCase(from)) {
            stop();
            Intent environmentPage = new Intent(WallExplanation.this, CiOps.class);
            environmentPage.putExtra("audio_in", audio_in);
            startActivity(environmentPage);
            finish();
        }
        else if (("cutmodels").equalsIgnoreCase(from)) {
            stop();
            Intent environmentPage = new Intent(WallExplanation.this, CutModels.class);
            environmentPage.putExtra("audio_in", audio_in);
            startActivity(environmentPage);
            finish();
        }
        else if (("founder").equalsIgnoreCase(from)) {
            stop();
            Intent environmentPage = new Intent(WallExplanation.this, GroupTourActivity.class);
            environmentPage.putExtra("audio_in", audio_in);
            startActivity(environmentPage);
            finish();
        }
        else if (("colonels").equalsIgnoreCase(from)) {
            stop();
            Intent environmentPage = new Intent(WallExplanation.this, ColonelsCentreCommandants.class);
            environmentPage.putExtra("audio_in", audio_in);
            startActivity(environmentPage);
            finish();
        }
        else{
            stop();
            Intent environmentPage = new Intent(WallExplanation.this, SequencialTourActivity.class);
            environmentPage.putExtra("audio_in", audio_in);
            startActivity(environmentPage);
            finish();
        }
    }
    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            WallExplanation.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewpager.getCurrentItem() < count-1) {
                        viewpager.setCurrentItem(viewpager.getCurrentItem() + 1);
                    } else {
                        viewpager.setCurrentItem(0);
                    }
                }
            });
        }
    }
}




