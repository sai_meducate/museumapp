package museum.m_educate.com.museum;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by macbookpro on 10/10/17.
 */

public class SequencialTourActivity extends AppCompatActivity {

    private final String wall_names[] = {
            "Wall 1",
            "Archives ",
            "Wall 2",
            "Wall 3",
            "Wall 4",
            "Wall 5",
            "Wall 6",
            "Wall 7",
            "Wall 8",
            "Wall 9",
            "Wall 10",
            "Wall 11",
            "Wall 12",
            "Wall 13",
            "Wall 14",
            "Wall 15-16",
            "Wall 17",
            "Wall 18",
            "Wall 19",
            "Wall 20",
            "Wall 21",
            "Wall 22",
            "Wall 23-30",
            "Wall 31",
            "Wall 32",
            "Wall 33",
            "Wall 34",
            "Wall 35",
            "Wall 36",
            "Wall 37",
            "Wall 38",
            "Wall 39",
            "Wall 40",
            "Wall 41",
            "Wall 42",
            "Wall 43",
            "Wall 44",
            "Wall 45",
            "Wall 46",
            "Wall 47",
            "Wall 48",
            "Wall 49",
            "Wall 50",
            "Wall 51",
            "Wall 52",
            "Wall 53",
            "Wall 54",
            "Wall 55",
            "Wall 56",
            "Wall 57",
            "Wall 58",
            "Wall 59",
            "Wall 60",
            "Wall 61",
            "Wall 62",
            "Wall 63",
            "Wall 64",
            "Wall 65",
            "Wall 66",
            "Wall 67",
            "Wall 68",
            "Wall 69",
            "Wall 70",
            "Wall 71",
            "Wall 72",
            "Wall 73",
            "Wall 74",
            "Wall 75",
            "Wall 76-77",
            "Wall 78-81",
            "Wall 82"    };

    private final int wall_image_urls[] = {
            R.drawable.wall_one_thumb,
            R.drawable.no_preview,
            R.drawable.wall_two,
            R.drawable.wall_four,
            R.drawable.wall_five,
            R.drawable.no_preview,
            R.drawable.no_preview,
            R.drawable.no_preview,
            R.drawable.no_preview,
            R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
            R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,R.drawable.no_preview,
    };

//    int num [][]= { {R.drawable.garuda_emblem,R.drawable.indonesian_emblem}, {R.drawable.garuda_emblem,R.drawable.indonesian_emblem}};

    private String audio_in = "Hindi";
    Button back, home,exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sequencial_tour);
        try {
            audio_in = getIntent().getStringExtra("audio_in");
            Log.d("audio", "onCreate: " + audio_in);

        } catch (Exception e) {
            Toast.makeText(this, "Audio not selected", Toast.LENGTH_LONG).show();
        }
        initViews();
        back = (Button) findViewById(R.id.back_button);
        home = (Button) findViewById(R.id.cont);
        exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                System.exit(0);
                AlertDialog.Builder alert = new AlertDialog.Builder(SequencialTourActivity.this);

                alert.setTitle("Are you sure you want to close this tour ?");

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }


                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(SequencialTourActivity.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent environmentPage = new Intent(SequencialTourActivity.this, GoroupSequenceTourActivity.class);
                environmentPage.putExtra("audio_in", audio_in);
                startActivity(environmentPage);
                finish();
            }
        });
    }

    private void initViews() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recyclerView.setLayoutManager(layoutManager);

        ArrayList<Walls> walls = prepareData();
        DataAdapter adapter = new DataAdapter(getApplicationContext(), walls, audio_in);
        recyclerView.setAdapter(adapter);

    }

    private ArrayList<Walls> prepareData() {

        ArrayList<Walls> wall = new ArrayList<>();
        for (int i = 0; i < wall_names.length; i++) {
            Walls walls = new Walls();
            walls.setWall(wall_names[i]);
            walls.setImage_url(wall_image_urls[i]);
//            walls.setSliders(num[i]);
            wall.add(walls);
        }
        return wall;
    }


    @Override
    public void onBackPressed() {
        Intent environmentPage = new Intent(this, GoroupSequenceTourActivity.class);
        environmentPage.putExtra("audio_in", audio_in);
        startActivity(environmentPage);
        finish();
    }
}

